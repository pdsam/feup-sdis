import java.io.Serializable;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

public class ChunkData implements Serializable{

	private String senderId;
	private String fileId;
	private Long chunkNo;
	private int desiredReplicationDegree;
	private ConcurrentHashMap<String, Boolean> owners = new ConcurrentHashMap<String, Boolean>();
	private int size;

	public ChunkData(String senderId, String fileId, long chunkNo, int desiredReplicationDegree, int size){
		this.senderId = senderId;
		this.fileId = fileId;
		this.chunkNo = chunkNo;
		this.desiredReplicationDegree = desiredReplicationDegree;
		this.size = size;
	}

	public String getSenderId(){
		return senderId;
	}

	public String getFileId(){
		return fileId;
	}

	public Long getChunkNo(){
		return chunkNo;
	}

	public int getSize(){
		return size;
	}

	public String getUniqueIdentifier(){
		return fileId+"-"+chunkNo;
	}

	public int getApparentReplicationDegree(){
		return owners.size();
	}

	public int addApparentReplicator(String id){
		owners.putIfAbsent(id, Boolean.TRUE);
		return getApparentReplicationDegree();
	}

	public int removeApparentReplicator(String id){
		owners.remove(id);
		return getApparentReplicationDegree();
	}

	public int getDesiredReplicationDegree(){
		return desiredReplicationDegree;
	}

	public String toString(){
		return getUniqueIdentifier();
	}

	public boolean isReplicator(String id){
		return owners.get(id) != null;
	}

	public Set<String> getReplicators(){
		return new HashSet<String>(Collections.list(owners.keys()));
	}

	public ConcurrentHashMap<String, Boolean> getOwners() {
		return this.owners;
	}

}
