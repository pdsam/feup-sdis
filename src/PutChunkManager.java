import java.io.ByteArrayOutputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.concurrent.Semaphore;
import java.nio.file.StandardOpenOption;
import java.nio.channels.AsynchronousFileChannel;
import java.nio.channels.CompletionHandler;
import java.nio.ByteBuffer;
import java.nio.file.Paths;
import java.io.IOException;
import java.util.concurrent.Future;
import java.util.ArrayList;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;


public class PutChunkManager {
	private ChordPeer peer;
	private ConcurrentHashMap<String, PutChunkProcess> processes = new ConcurrentHashMap<>();

	private class PutChunkProcess implements Process, CompletionHandler<Integer, Long>{
		private AsynchronousFileChannel fileChannel;
		private BackedupFile instance;
		private int currentRetry = 0;
		private Future nextFunction = null;
		private Semaphore terminationFlag = new Semaphore(0);
		private ByteBuffer buffer;
		private ArrayList<Long> chunksToBeProcessed;
		private Runnable runOnTermination;
		private int statusCode = 0;
		private boolean isStopped = false;

		private String fileId;

		public PutChunkProcess(BackedupFile instance, Runnable runOnTermination){
			this.instance = instance;
			this.runOnTermination = runOnTermination;
		}

		public int returnStatus(){
			return statusCode;
		}

		public synchronized void run(){
			nextFunction = null;
			if(isStopped)
				return;

			chunksToBeProcessed = instance.chunksWithoutDesiredReplication();
			statusCode = chunksToBeProcessed.size();
			if(chunksToBeProcessed.size() == 0){
				stop(false);
				return;
			}

			if(currentRetry >= Constants.MAX_RETRIES){
				stop(false);
				return;
			}

			if(fileChannel == null){
				try{
					fileChannel = AsynchronousFileChannel.open(Paths.get(instance.getFilePath()), StandardOpenOption.READ);
				}
				catch(IOException e){
					Logger.error("Problem in opening file from disk, PUTCHUNK");
					stop(true);
					return;
				}
			}

			if(buffer == null)
				buffer = ByteBuffer.allocate(Constants.CHUNK_MAX_SIZE_BYTES);

			Long cur = chunksToBeProcessed.remove(0);
			readPortion(cur);
		}

		public void waitTermination(){
			terminationFlag.acquireUninterruptibly();
			terminationFlag.release();
		}

		private void readPortion(Long portion){
			try{
				buffer.clear();
				fileChannel.read(buffer, portion*Constants.CHUNK_MAX_SIZE_BYTES, portion, this);
			}
			catch(Throwable e){
				Logger.error("Reading for PUTCHUNK failed");
				stop(true);
				return;
			}
		}

		public boolean isStopped(){
			return isStopped;
		}

		public synchronized void stop(boolean kill){
			if(isStopped)
				return;

			if(kill)
				statusCode = -1;

			isStopped = true;
			processes.remove(instance.getId());
			terminationFlag.release();

			runOnTermination.run();

			if(fileChannel != null){
				try{
					fileChannel.close();
				}
				catch(IOException e){}
			}

			if(nextFunction != null)
				nextFunction.cancel(kill);
		}

		public void completed(Integer result, Long cur){
			if(result == -1){
				Logger.error("Got EOF on chunk " + cur);
				return;
			}

			int apparentReplication = instance.getApparentReplicationDegreeOfChunk(cur);
			if(apparentReplication == -1){
				Logger.error("Wrong chunk number de-sync problem");
			}
			else if(apparentReplication < instance.getDesiredReplicationDegree()){
				String chunkId = Peer.hashAlgorithm((fileId + cur).getBytes());
				int TTL = 5*instance.getDesiredReplicationDegree() + (currentRetry*2);

				byte[] contents = new byte[result];
				System.arraycopy(buffer.array(), 0, contents, 0, result);
				PutChunkPayload payload = new PutChunkPayload(
						instance.getSenderId(),
					   	TTL,
					   	instance.getDesiredReplicationDegree(),
						apparentReplication,
						instance.getId(),
						cur,
						contents
				);

				//obtain peer address
				PutChunkManager.this.peer.find_successor(chunkId, (finger) -> {


					Finger bestFinger = finger;
					if(finger == null){
						Logger.warn("PUTCHUNK: Got null finger for chunk " + cur);
						return null;
					}
					else if(finger.equals(PutChunkManager.this.peer.getOwnFinger())){
						Logger.info("PUTCHUNK: Finger that I got was mine, will search nearest finger");
						Finger nearestFinger = PutChunkManager.this.peer.findEarliestFreeFinger();
						if(nearestFinger == null){
							Logger.warn("PUTCHUNK: There's no finger nearby to send chunk " + cur);
							return null;
						}
						Logger.info("PUTCHUNK: Found nearest finger " + nearestFinger);
						bestFinger = nearestFinger;
					}
					
					Logger.info("PUTCHUNK: Nearest finger of " + chunkId + " is " + bestFinger);
					//connect and send
					PutChunkManager.this.peer.connectToPeer(bestFinger.getAddress(), sslConnectionManager -> {
						Message message = new Message(MessageType.PUTCHUNK, payload);
						ByteArrayOutputStream out = new ByteArrayOutputStream();
						try {
							ObjectOutputStream os = new ObjectOutputStream(out);
							os.writeObject(message);
						} catch (IOException e) {
							e.printStackTrace();
						}
						sslConnectionManager.write(ByteBuffer.wrap(out.toByteArray()), sslConnectionManager::close, ()->{});
						return null;
					});
					return null;
				});
			}

			if(chunksToBeProcessed.size() == 0){
			    //reschedule process
				int sleepTime = 5+(int)Math.pow(2, currentRetry);
				Logger.info("PUTCHUNK: Will sleep for: " + sleepTime);
				PutChunkManager.this.peer.addTaskWithDelay(this, sleepTime, TimeUnit.SECONDS);
				currentRetry++;
				return;
			}

			Long next = chunksToBeProcessed.remove(0);
			readPortion(next);
		}

		public void failed(Throwable result, Long obj){
			Logger.error("A problem occurred reading on PUTCHUNK");
			stop(true);
		}
	}

	public PutChunkManager(ChordPeer peer) {
		this.peer = peer;
	}

	public void stopProcess(BackedupFile putChunkProvider){
		PutChunkProcess res = processes.get(putChunkProvider.getId());
		if(res == null)
			return;
		res.stop(true);
	}

	public Process createProcess(BackedupFile putChunkInstance, Runnable runOnTermination){
		final PutChunkProcess process = new PutChunkProcess(putChunkInstance, runOnTermination);
		PutChunkProcess res = processes.computeIfAbsent(putChunkInstance.getId(), (k) -> process);
		if(process == res)
			res.run();
		return res;
	}

	public Process createProcess(BackedupFile putChunkProvider){
		return createProcess(putChunkProvider, () -> {});
	}

	public boolean processExists(String fileId){
		return processes.get(fileId) != null;
	}

}
