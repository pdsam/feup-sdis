import java.io.File;
import java.util.concurrent.ConcurrentHashMap;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.function.Function;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Collections;


public class BackedupFile implements Serializable{
	private int desiredReplicationDegree;
	private ConcurrentHashMap<Long, ChunkData> chunkStatus = new ConcurrentHashMap<Long, ChunkData>();
	private String id;
	private String filePath;
	private String fileName;
	private static transient MessageDigest md = null;
	private long lastModified;
	private long size;
	private long numChunks;
	private String senderId;


	public BackedupFile(File file, String peerId, int desiredReplicationDegree) throws NoSuchAlgorithmException, FileTooBigException{
		this.filePath = file.getAbsolutePath();
		this.fileName = file.toPath().getFileName().toString();
		this.lastModified = file.lastModified();
		this.size = file.length();
		this.desiredReplicationDegree = desiredReplicationDegree;
		this.senderId = peerId;
		this.id = generateHash(senderId + this.filePath + this.lastModified);

		this.numChunks = (int)(this.size/Constants.CHUNK_MAX_SIZE_BYTES);
		this.numChunks += (this.size%Constants.CHUNK_MAX_SIZE_BYTES != 0 ) ? 1 : 0;

		if(this.numChunks >= Constants.MAX_CHUNK_NO)
			throw new FileTooBigException();

		for(long i =0;i<this.numChunks; i++){
			chunkStatus.putIfAbsent(i, new ChunkData(senderId, this.id, i, desiredReplicationDegree, 0));
		}

	}

	public String getSenderId(){
		return senderId;
	}

	public int getDesiredReplicationDegree(){
		return desiredReplicationDegree;
	}

	private String generateHash(String tbe) throws NoSuchAlgorithmException{
		if(this.md == null)
			this.md = MessageDigest.getInstance("SHA-256");
		this.md.reset();
		byte[] hash = md.digest(tbe.getBytes());
		StringBuilder sb = new StringBuilder();
		for (byte b : hash) {
			sb.append(String.format("%02X", b));
		}
		return sb.toString();
	}
	
	public String getId(){
		return this.id;
	}

	public boolean allChunksWithDesiredReplication(){
		for(ChunkData element : chunkStatus.values())
			if(element.getApparentReplicationDegree() < this.desiredReplicationDegree)
				return false;
		return true;
	}

	public boolean allChunksZeroReplication(){
		int maxApparentReplication = 0;
		for(ChunkData element : chunkStatus.values()){
			maxApparentReplication = Math.max(maxApparentReplication, element.getApparentReplicationDegree());
			if(maxApparentReplication != 0)
				return false;
		}
		return true;
	}

	public synchronized ArrayList<Long> chunksWithoutDesiredReplication(){
		ArrayList<Long> res = new ArrayList<Long>();

		chunkStatus.forEach( (k,v) -> { 
			if(v.getApparentReplicationDegree() < this.desiredReplicationDegree)
				res.add(k);
		});
		return res;
	}

	public synchronized int getApparentReplicationDegreeOfChunk(long chunkNo){
		ChunkData chData = chunkStatus.get(chunkNo);
		if(chData == null)
			return -1;

		return chData.getApparentReplicationDegree();
	}

	public synchronized ArrayList<Long> chunksThatIdIsReplicator(String id){
		ArrayList<Long> res = new ArrayList<Long>();
		for(ChunkData element : chunkStatus.values())
			if(element.isReplicator(id))
				res.add(element.getChunkNo());
		return res;
	}

	public synchronized ArrayList<String> getReplicatorsForChunk(long chunkNo){
		ArrayList<String> res = new ArrayList<String>();
		
		ChunkData chData = chunkStatus.get(chunkNo);
		if(chData == null)
			return res;

		return new ArrayList<String>(chData.getReplicators());
	}

	public synchronized int addChunkStored(ChunkData incoming){

		ChunkData chData = chunkStatus.get(incoming.getChunkNo());
		if(chData == null){
			Logger.warn("Invalid chunkNo was received as stored");
			return -1;
		}

		Logger.info("Got STORED  response for " + chData.getUniqueIdentifier());
		return chData.addApparentReplicator(incoming.getSenderId());
	}

	public synchronized int removeChunkStored(ChunkData incoming){

		ChunkData chData = chunkStatus.get(incoming.getChunkNo());
		if(chData == null){
			Logger.warn("Invalid chunkNo was received for removal");
			return -1;
		}

		Logger.info("Got REMOVED  response for " + chData.getUniqueIdentifier());
		return chData.removeApparentReplicator(incoming.getSenderId());
	}

	public String getFilePath(){
		return filePath;
	}

	public String getFileName(){
		return fileName;
	}

	public int numChunks(){
		return chunkStatus.size();
	}

	public void runOnAllChunks(Function<ChunkData, Void> func){
		
		chunkStatus.forEach(
				(k,v) ->
				{
					func.apply(v);
				}
				);

	}

	public ChunkData getChunkData(Long chunkNo){
		return chunkStatus.get(chunkNo);
	}
}
