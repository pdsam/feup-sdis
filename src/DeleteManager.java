import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.nio.ByteBuffer;
import java.util.concurrent.ConcurrentHashMap;

import java.io.ByteArrayOutputStream;
import java.io.ObjectOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ObjectInputStream;
import java.io.EOFException;
import java.util.Set;
import java.util.Collections;
import java.util.HashSet;

public class DeleteManager implements Runnable {

    private ConcurrentHashMap<String, DeleteProcess> processes = new ConcurrentHashMap<String, DeleteProcess>();
    private ChordPeer peer;
    private class DeleteProcess implements Process{

        private BackedupFile backedupFile;

        public DeleteProcess(BackedupFile backedupFile){
            this.backedupFile = backedupFile;
        }


        @Override
        public void waitTermination() {

        }

        @Override
        public void stop(boolean kill) {
            DeleteManager.this.processes.remove(backedupFile);
        }

        @Override
        public int returnStatus() {
            return 0;
        }

        @Override
        public void run() {
            if (backedupFile.allChunksZeroReplication()) {
                stop(false);
                return;
            }

			Set<String> replicators = new HashSet<String>();
			backedupFile.runOnAllChunks((chData)->{
					replicators.addAll(chData.getReplicators());
					return null;
			});

			replicators.stream().forEach(
					(cur) -> { deleteFunction(cur); } );

        }

		public boolean isStopped(){
			return false;
		}


		private class DeleteConnectionProcess implements Process{

			private SSLConnectionManager connection;
			private ResizableBuffer buffer = new ResizableBuffer();
			private String peerId;
			public DeleteConnectionProcess(String peerId, SSLConnectionManager connection){
				this.connection = connection;
				this.peerId = peerId;
			}

			public void waitTermination(){}
			public void stop(boolean kill){
				connection.close();
				DeleteProcess.this.connections.remove(peerId);
			}
			public int returnStatus(){return 0;}
			public boolean isStopped(){return false;}
			public void run(){
				connection.read(buffer,
						()->{
							
						Message msg = null;
						try{
							ByteArrayInputStream in = new ByteArrayInputStream(buffer.array(), 0, buffer.position());
							ObjectInputStream is = new ObjectInputStream(in);
							msg = (Message)is.readObject();
						}
						catch(EOFException e){
							//object is bigger will read more :)
							this.run();
							return;
						}
						catch(IOException e){
							Logger.error("DELETE: Problem de-serializing object " + buffer);
							stop(true);
							return;
						}
						catch(ClassNotFoundException e){
							Logger.error("DELETE: Could not find class for de-serialization");
							stop(true);
							return;
						}

						if(msg.getType() != MessageType.REMOVED_DELETE){
							Logger.warn("DELETE: Got wrong answer " + msg.getType().name());
							stop(true);
							return;
						}

						DeleteProcess.this.backedupFile.removeChunkStored(new ChunkData(peerId, DeleteProcess.this.backedupFile.getId(), (Long)msg.getBody(), 0, 0));
						buffer.clear();
						run();
			},
						()->{stop(true);});

			}
		}

		ConcurrentHashMap<String, DeleteConnectionProcess> connections = new ConcurrentHashMap<String, DeleteConnectionProcess>();


		private void deleteFunction(String hash){

			DeleteConnectionProcess process = connections.get(hash);
			if(process != null){
				connections.remove(hash);
				process.stop(true);
			}
			DeleteManager.this.peer.find_successor(hash, finger ->{

				if(!finger.getId().equals(hash)){
					Logger.error("DELETE: got wrong finger for " + hash + " got " + finger);
					return null;
				}
				DeleteManager.this.peer.connectToPeer(finger.getAddress(),sslConnectionManager -> {
					Message message = new Message(MessageType.DELETE, new DeletePayload(backedupFile.getId(), backedupFile.chunksThatIdIsReplicator(hash) ));
					ByteArrayOutputStream out = new ByteArrayOutputStream();
					try{
						ObjectOutputStream os = new ObjectOutputStream(out);
						os.writeObject(message);
						sslConnectionManager.write(ByteBuffer.wrap(out.toByteArray()), ()->{
							
							DeleteConnectionProcess connProc = new DeleteConnectionProcess(hash, sslConnectionManager);
							connections.putIfAbsent(hash, connProc);
							DeleteManager.this.peer.addTask(connProc);
						
						}, ()->{});
					} catch(IOException e){
						e.printStackTrace();
					}
					return  null;
				});
				return null;
			});



		}

    }

    public DeleteManager(ChordPeer peer){
        this.peer = peer;
    }

    public synchronized void run(){
        processes.forEach((k,v)->{
            v.run();
        });
    }

    public synchronized void deleteFile(BackedupFile backedupFile){
        Logger.info("Queuing file deletion: " + backedupFile.getId());
        DeleteProcess deleteProcess = new DeleteProcess(backedupFile);
        processes.putIfAbsent(backedupFile.getId(),deleteProcess);
    }


    /**
     * If the chunk gets stored again before getting deleted
     * @param backedupFile
     */
    public synchronized void fileBackedup(BackedupFile backedupFile){
        processes.remove(backedupFile.getId());
    }

}
