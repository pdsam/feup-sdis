import java.rmi.Remote;
import java.rmi.RemoteException;
public interface ChordRemote extends Remote{
    String state() throws RemoteException;
    int backup(String fileName, int desiredReplicationDegree) throws RemoteException;
    int restore(String filePath) throws RemoteException;
    void delete(String filePath) throws RemoteException;

	void reclaim(long newSize) throws RemoteException;
}
