import java.net.InetSocketAddress;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class ChordClient {
    public static void main(String[] args) {
        if (args.length < 2) {
            System.out.println("Parameters: <PEER_ID> (COMMAND)");
            System.out.println("Command:");
            System.out.println("\t- STATE");
        }

        ChordRemote peer;
        try {
            Registry reg = LocateRegistry.getRegistry(1099);

			String[] l = reg.list();
			for(String s : l)
				System.out.println("Available: " + s);
            peer = (ChordRemote) reg.lookup(args[0]);
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }

        try {
			int res = 0;
            switch (args[1]) {
                case "STATE":
                    System.out.println(peer.state());
                    break;
				case "BACKUP":
					res = peer.backup(args[2], Integer.valueOf(args[3]));
					if(res == 0)
						System.out.println("All chunks were replicated with desired replication degree");
					else
						System.out.println("" + res + " chunk(s) couldn't be replicated with desired replication degree");
					break;
                case "RESTORE":
                    if(args.length < 3){
                        System.err.println("RESTORE needs the file name");
                        break;
                    }
                    res = peer.restore(args[2]);
					if(res < 0)
						System.out.println("There was problem restoring the file");
					else
						System.out.println("File was restored with success!");
                    break;
				case "RECLAIM":
					peer.reclaim(Long.valueOf(args[2]));
					break;
                case "DELETE":
                    peer.delete(args[2]);
                    break;
                default:
                    System.err.println("Invalid operation.");
            }
        } catch (RemoteException e) {
			System.out.println(e.toString());
        }
    }
}
