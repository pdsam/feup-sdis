import java.io.Serializable;

public class Message implements Serializable {

    private MessageType messageType;
    private Object body;

    Message(MessageType messageType, Object body){
        this.messageType = messageType;
        this.body = body;
    }


	public MessageType getType(){
		return messageType;
	}

	public Object getBody(){
		return body;
	}
}
