import java.io.Serializable;

public class YeetPayload implements Serializable {
    private String peerID;
    private String fileId;
    private long chunkNumber;

    public YeetPayload(String originalId, String fileId, long chunkNumber) {
        this.peerID = originalId;
        this.fileId = fileId;
        this.chunkNumber = chunkNumber;
    }

    public String getPeerID() {
        return peerID;
    }

    public String getFileId() {
        return fileId;
    }

    public long getChunkNumber() {
        return chunkNumber;
    }
}
