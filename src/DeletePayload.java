import java.io.Serializable;
import java.util.ArrayList;

public class DeletePayload implements Serializable{

	private String fileId;
	private ArrayList<Long> chunksToDelete;

	public DeletePayload(String fileId, ArrayList<Long> chunksToDelete){
		this.fileId = fileId;
		this.chunksToDelete = chunksToDelete;
	}

	public boolean missingChunks(){
		return chunksToDelete.size() != 0;
	}

	public Long getChunk(){
		return chunksToDelete.remove(0);
	}

	public void removeElement(Long element){
		chunksToDelete.remove(element);
	}

	public String getFileId(){
		return fileId;
	}

}
