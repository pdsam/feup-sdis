import java.nio.ByteBuffer;
import java.io.Serializable;

public class PutChunkPayload implements Serializable{
    private final String ownerId;
    private int TTL;
    private final int desiredReplication;
    private int apparentReplication;
    private final String fileId;
    private final Long chunkNo;
    private final byte[] content;

    public PutChunkPayload(String ownerId, int TTL, int desiredReplication, int apparentReplication, String fileId, Long chunkNo, byte[] content) {
        this.ownerId = ownerId;
        this.TTL = TTL;
        this.desiredReplication = desiredReplication;
        this.apparentReplication = apparentReplication;
		this.fileId = fileId;
		this.chunkNo = chunkNo;
		this.content = content;
    }

    public String getOwnerId() {
        return ownerId;
    }

    void decrementTTL() {
        this.TTL--;
    }

    public int getTTL() {
        return TTL;
    }

    public int getDesiredReplicationDegree() {
        return desiredReplication;
    }

    public int getApparentReplicationDegree() {
        return apparentReplication;
    }

	public void incrementApparentReplicationDegree(){
		apparentReplication += 1;
	}

    public byte[] getContent() {
        return content;
    }

	public String getFileId(){
		return fileId;
	}

	public long getChunkNo(){
		return chunkNo;
	}

	public String toString(){
		return ownerId + " " + fileId+"-"+chunkNo;
	}
}
