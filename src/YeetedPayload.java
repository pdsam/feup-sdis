import java.io.Serializable;

public class YeetedPayload implements Serializable {
    private String peerId;
    private String fileId;
    private long chunkNumber;

    public YeetedPayload(String peerId, String fileId, long chunkNumber) {
        this.peerId = peerId;
        this.fileId = fileId;
        this.chunkNumber = chunkNumber;
    }

    public String getPeerId() {
        return peerId;
    }

    public String getFileId() {
        return fileId;
    }

    public long getChunkNumber() {
        return chunkNumber;
    }
}
