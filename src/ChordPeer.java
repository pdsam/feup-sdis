import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.concurrent.Future;

public interface ChordPeer extends IPeer{

	String getId();
    void notify(Finger chordPred);
    void fix_fingers();
    void check_predecessor();

    void find_successor(String key, Function<Finger, Void> callback);
    void find_successor(String key, Function<Finger, Void> callback, Runnable runOnFailure);
	Finger getPredecessor();

	Future addTaskWithDelay(Runnable r, long delay, TimeUnit tu);
    void connectToPeer(InetSocketAddress peerAddress, Function<SSLConnectionManager, Void> callback);

    void handlePutChunk(PutChunkPayload payload);
	public Finger findEarliestFreeFinger();
	public Finger getOwnFinger();
	public void stored(SSLConnectionManager connection, StoredPayload stored);
	public void getchunk(Function<Object, Void> answer, ChunkData chData);

	void removed(RemovedPayload payload);
	public void deleteChunks(DeletePayload payload, Function<Long, Void> sender);
	public boolean gotBegged(ChunkData chData);
}
