import java.io.Serializable;

public class RemovedPayload implements Serializable{
	
	public String ownerId;
	public String senderId;
	public String fileId;
	public Long chunkNo;

	public RemovedPayload(ChunkData chData, String senderId){
		this.ownerId = chData.getSenderId();
		this.senderId = senderId;

		this.fileId = chData.getFileId();
		this.chunkNo = chData.getChunkNo();
	}


	public String getOwnerId(){
		return ownerId;
	}

	public ChunkData toChunkData(){
		return new ChunkData(senderId, fileId, chunkNo, 0, 0);
	}

}
