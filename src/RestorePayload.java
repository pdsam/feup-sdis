import java.io.Serializable;
import java.net.InetSocketAddress;

public class RestorePayload implements Serializable{
    private final Finger owner;
    private final String fileId;

    public RestorePayload(Finger owner, String fileId) {
        this.owner = owner;
		this.fileId = fileId;
    }

    public String getOwnerId() {
        return owner.getId();
    }

    public InetSocketAddress getOwnerAddress() {return owner.getAddress(); }

	public String getFileId(){
		return fileId;
	}

	public String toString(){
		return getOwnerId() + " " + fileId;
	}
}
