import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousFileChannel;
import java.nio.channels.CompletionHandler;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.concurrent.*;
import java.util.function.Function;
import java.io.ByteArrayOutputStream;
import java.io.ObjectOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ObjectInputStream;

import java.util.Set;
import java.util.Collections;
import java.util.HashSet;
import java.util.ArrayList;

import java.io.EOFException;
public class RestoreManager{

	private class RestoreProcess implements Process {

		private class RestoreProcessConnection implements Process{

			boolean isStopped = false;
			String peerId;
			SSLConnectionManager curConnection = null;
			Function<String, Long> requestAskChunk = null;
			Long currentChunk = null;
			ResizableBuffer resBuffer = new ResizableBuffer(18000);

			public RestoreProcessConnection(String peerId, SSLConnectionManager curConnection, Function<String,Long> requestAskChunk){
				this.peerId = peerId;
				this.requestAskChunk = requestAskChunk;
				this.curConnection = curConnection;
			}

			public void waitTermination(){}

			public synchronized void stop(boolean kill){
				
				if(isStopped)
					return;

				isStopped = true;	
				if(curConnection != null)
					curConnection.close();

				if(currentChunk != null)
					RestoreProcess.this.chunksProcessing.remove(currentChunk);
				RestoreProcess.this.connections.remove(peerId);
			}

			public int returnStatus(){ return 0; }

			public void readChunk(){

				Message msg = null;

				try{
					ByteArrayInputStream in = new ByteArrayInputStream(resBuffer.array(), 0, resBuffer.position());
					ObjectInputStream is = new ObjectInputStream(in);
					msg = (Message)is.readObject();
				}
				catch(EOFException e){
					//object is bigger will read more :)
					curConnection.read(resBuffer, ()->{readChunk();}, ()->{stop(true);});
					return;
				}
				catch(IOException e){
					Logger.error("Problem de-serializing object " + resBuffer);
					stop(true);
					return;
				}
				catch(ClassNotFoundException e){
					Logger.error("Could not find class for de-serialization");
					stop(true);
				}

				if(msg.getType() != MessageType.CHUNK && msg.getType() != MessageType.REMOVED){
					Logger.warn("RESTORE: Got invalid message type " + msg.getType().name());
					stop(true);
					return;
				}

				if(msg.getType() == MessageType.REMOVED){
					String fileId = RestoreProcess.this.backedupFile.getId();
					RestoreProcess.this.backedupFile.removeChunkStored(new ChunkData(peerId, fileId, currentChunk, 0, 0));
				}
				else{
					RestoreProcess.this.receivedChunk(currentChunk, ((ChunkPayload)msg.getBody()).getBuffer());
				}
				run();
			}

			public void run(){ 
				if(isStopped)
					return;

				if(curConnection == null){
					stop(false);
					return;
				}


				currentChunk = requestAskChunk.apply(peerId);
				if(currentChunk == -1){
					stop(false);
					return;
				}

				resBuffer.clear();

				Runnable runOnSuccess = ()->
				{
					curConnection.read(resBuffer,
							()->{readChunk();}
					,
					()->{stop(false);}
					);

				};
				sendObject(new Message(MessageType.GETCHUNK, new ChunkData(RestoreProcess.this.backedupFile.getSenderId(), RestoreProcess.this.backedupFile.getId(), currentChunk, 0, 0)),
						runOnSuccess
						,
						()->{stop(false);}
						);

			}

			private void sendObject(Object o, Runnable runOnSuccess, Runnable runOnFailure){

					try{
						ByteArrayOutputStream out = new ByteArrayOutputStream();
						ObjectOutputStream os = new ObjectOutputStream(out);
						os.writeObject(o);
						curConnection.write(ByteBuffer.wrap(out.toByteArray()), runOnSuccess, runOnFailure);	
					}
					catch(IOException e){
						Logger.error("Problem serializing object");
						stop(true);
					}
			}

			public boolean isStopped(){
				if(curConnection == null)
					return true;

				if(curConnection.isOver())
					return true;
				return isStopped;
			}


		}

		private BackedupFile backedupFile;
		private ConcurrentHashMap<Long, Boolean> chunksLeft;
		private ConcurrentHashMap<Long, Boolean> chunksProcessing;
		private ConcurrentHashMap<String, RestoreProcessConnection> connections = new ConcurrentHashMap<String, RestoreProcessConnection>();

		private boolean isStopped;
		private Semaphore sem = new Semaphore(0);
		private int statusCode = 0;
		protected Future nextFunction = null;
		private int retries;

		RestoreProcess(BackedupFile backedupFile){
			this.backedupFile = backedupFile;
			this.chunksLeft = new ConcurrentHashMap<Long, Boolean>();
			long numChunks = backedupFile.numChunks();
			for(long i =0;i<numChunks;i++)
				chunksLeft.put(i, true);
			this.chunksProcessing = new ConcurrentHashMap<Long, Boolean>();
			retries = 0;
		}

		public void waitTermination(){
			sem.acquireUninterruptibly();
			sem.release();
		}

		public boolean isStopped(){
			return isStopped;
		}

		public void stop(boolean kill){
			if(isStopped)
				return;

			isStopped = true;
			sem.release();
			if(kill){
				statusCode = -1;
				File file = new File(getRestoredFilePath().toString());
				file.delete();
			}

			if(nextFunction != null)
				nextFunction.cancel(kill);

			RestoreManager.this.processes.remove(backedupFile.getId());
			if(nextFunction != null)
				nextFunction.cancel(true);

		}

		public int returnStatus(){
			return 0;
		}

		public void run(){

			if(isStopped)
				return;
			if(retries >= Constants.MAX_RETRIES){
				stop(true);
				return;
			}

			if(chunksLeft.size() == 0){
				Logger.info("All chunks have been restored");
				stop(false);
				return;
			}

			if(retries != 0)
				Logger.info("RESTORE: " + backedupFile.getId() + " current retry " + retries);

			
			Set<String> replicators = new HashSet<String>();
			backedupFile.runOnAllChunks((chData)->{
					replicators.addAll(chData.getReplicators());
					return null;
			});


			//zombie killer
			Collections.list(connections.keys()).stream().forEach(
					(cur)->
					{
						if(!replicators.contains(cur)){
							connections.remove(cur);
						}
						else{
							RestoreProcessConnection conn = connections.get(cur);
							if(conn.isStopped()){
								conn.stop(true);
								connections.remove(cur);
							}

						}

					});

			replicators.stream().forEach((cur) -> {

				RestoreManager.this.chordPeer.find_successor(cur,

						(finger)->{
							if(!finger.getId().equals(cur)){
								Logger.warn("Couldn't find " + cur + " got " + finger);
								return null;
							}

							 RestoreManager.this.chordPeer.connectToPeer(
									 finger.getAddress(),

									 (connection)->
									 {

										 RestoreProcessConnection proc = new RestoreProcessConnection(cur, connection, (inId) -> { return getChunkForPeer(inId); });

										 RestoreProcessConnection res = connections.computeIfAbsent(cur, (k) -> { return proc;});
										 if(res == proc){
											 Logger.info("Added process for connection with " + cur);
											 RestoreManager.this.chordPeer.addTask(proc);
										 }
										 return null;
									 }
									 );
							 return null;

						});

			});

			retries++;
			int sleepTime = chunksLeft.size()*2;
			Logger.info("RESTORE: " + backedupFile.getId() + " will sleep " + sleepTime);
			nextFunction = RestoreManager.this.chordPeer.addTaskWithDelay(this, sleepTime, TimeUnit.SECONDS);
		}


		public synchronized Long getChunkForPeer(String peerId){

			ArrayList<Long> chunks =  backedupFile.chunksThatIdIsReplicator(peerId);
			for(Long entry : chunks){
				if(chunksProcessing.get(entry) != null)
					continue;
				if(chunksLeft.get(entry) == null)
					continue;

				chunksProcessing.computeIfAbsent(entry, (k)->{return true;});
				return entry;
			}

			return (long)-1;
		}

		public boolean getIsStopped(){
			return this.isStopped;
		}

		public Path getRestoredFilePath(){
			return Paths.get(RestoreManager.this.chordPeer.restoreDir() + backedupFile.getFileName() + ".restored");
		}

		public void removeChunk(long chunkNo) {
			chunksLeft.remove(chunkNo);
		}

		public int getChunkListSize() {
			return chunksLeft.size();
		}


		public synchronized void receivedChunk(long chunkNo, byte[] body){

			Logger.success("RESTORE: "+ backedupFile.getId() + " received chunk to store: " + chunkNo);

			if(isStopped)
				return;
			try{ //Save received chunk
				AsynchronousFileChannel fileChannel = AsynchronousFileChannel.open(getRestoredFilePath(), StandardOpenOption.WRITE, StandardOpenOption.CREATE);
				ByteBuffer buf = ByteBuffer.wrap(body);
				fileChannel.write(buf, chunkNo * Constants.CHUNK_MAX_SIZE_BYTES, buf,

						new CompletionHandler<Integer, ByteBuffer>() {

							public void completed(Integer result, ByteBuffer attachment) {
								removeChunk(chunkNo);
								if (getChunkListSize() == 0) {
									stop(false);
								}

								try {
									fileChannel.close();
								} catch (IOException e) {
								}
							}

							public void failed(Throwable exception, ByteBuffer attachment) {
								Logger.error("Write chunk for RESTORE failed");
								stop(true);
								try {
									fileChannel.close();
								} catch (IOException e) {
								}
							}

						}
				);
			}
			catch(IOException e){
				Logger.error("IOException on writing RESTORE chunk");
				e.printStackTrace();
				return;
			}
		}


	}

	private ChordPeer chordPeer;
	private ConcurrentHashMap<String, RestoreProcess> processes;

	public RestoreManager(ChordPeer chordPeer){ //, BiFunction<ChunkData, Integer, Void> restoreFunction){
		this.chordPeer = chordPeer;
		this.processes = new ConcurrentHashMap<>();
		//this.restoreFunction = restoreFunction; BiFunction<ChunkData, Integer, Void> restoreFunction
	}

	public Process addProcess(BackedupFile backedup){
		RestoreProcess process = new RestoreProcess(backedup);

		final RestoreProcess tmp = process;
		RestoreProcess res = processes.computeIfAbsent(backedup.getId(), (k) -> {return tmp;});
		if(process == res)
			chordPeer.addTask(res);
		return res;
	}

}
