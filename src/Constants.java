public class Constants {

    public static final int MAX_RETRIES = 5;
    public static final int CHUNK_MAX_SIZE_BYTES = 1024*1024;
    public static final long MAX_CHUNK_NO = 4000000;
    public static int M = 32;
}
