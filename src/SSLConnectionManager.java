import java.io.FileInputStream;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.security.KeyStore;

import javax.net.ssl.KeyManager;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLEngine;
import javax.net.ssl.SSLEngineResult;
import javax.net.ssl.SSLEngineResult.HandshakeStatus;
import javax.net.ssl.SSLException;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import java.security.SecureRandom;

import java.nio.channels.AsynchronousSocketChannel;
import java.nio.channels.CompletionHandler;

import java.net.InetSocketAddress;
import java.security.NoSuchAlgorithmException;
import java.io.FileNotFoundException;
import java.security.KeyStoreException;
import java.security.cert.CertificateException;
import java.security.UnrecoverableKeyException;
import java.security.KeyManagementException;

public class SSLConnectionManager implements CompletionHandler<Integer, NeedUnwrapRead>{


	Runnable runOnHandShake;
	Runnable runOnFailure;
	AsynchronousSocketChannel socketChannel;
	String peerAddress = null;
	String handshakePrepend = "Handshake ";


	/*
	 *                    app data

                |           ^
                |     |     |
                v     |     |
           +----+-----|-----+----+
           |          |          |
           |       SSL|Engine    |
   wrap()  |          |          |  unwrap()
           | OUTBOUND | INBOUND  |
           |          |          |
           +----+-----|-----+----+
                |     |     ^
                |     |     |
                v           |

                   net data
	*/

	SSLEngine engine;
    protected ByteBuffer myAppData;
    protected ByteBuffer myNetData;

    protected ByteBuffer peerAppData;
    protected ByteBuffer peerNetData;
	private boolean client;


	private IPeer iPeer;

	public SSLConnectionManager(AsynchronousSocketChannel socketChannel, boolean client, IPeer iPeer, Runnable runOnHandShake){
		this.socketChannel = socketChannel;
		this.client = client;
		this.iPeer = iPeer;
		this.runOnHandShake = runOnHandShake;
		try{
			this.peerAddress = socketChannel.getRemoteAddress().toString();
		}
		catch(Exception e){
			Logger.error("Could not get peer address");
			this.peerAddress = "null";
		}
	}

	public SSLConnectionManager(AsynchronousSocketChannel socketChannel, boolean client, IPeer iPeer){
		this(socketChannel, client, iPeer, ()->{});
	}

	public void setRunOnHandshake(Runnable runOnHandShake){
		this.runOnHandShake = runOnHandShake;
	}

	public void setRunOnFailure(Runnable runOnFailure){
	}


	public void runConnection(){
		SSLContext context = null;
		try{
			context = SSLContext.getInstance("TLSv1.2");
		}
		catch(NoSuchAlgorithmException e){
			Logger.error("Could not find instance of ssl context");
			return;
		}

		try{
			context.init(
					createKeyManagers(new FileInputStream(client ? "client.keys" : "server.keys"), "123456", "123456"),
					createTrustManagers(new FileInputStream("truststore"), "123456"), new SecureRandom());
			InetSocketAddress isa = (InetSocketAddress)socketChannel.getRemoteAddress();
			engine = context.createSSLEngine(isa.getHostName(), isa.getPort());
			engine.setNeedClientAuth(true);
			engine.setUseClientMode(client);
		}
		catch(UnrecoverableKeyException e){
			Logger.error("Unrecoverable error occured");
			return;
		}
		catch(CertificateException e){
			Logger.error("Problem with certificate");
			return;
		}
		catch(KeyManagementException e){
			Logger.error("There was a problem managing the keys");
			return;
		}
		catch(NoSuchAlgorithmException e){
			Logger.error("Could not find algorithm");
			return;
		}
		catch(FileNotFoundException e){
			Logger.error("Could not find files");
			return;
		}
		catch(KeyStoreException e){
			Logger.error("Getting keystore");
			return;
		}
		catch(IOException e){
			Logger.error("Getting hostname");
			return;
		}

        SSLSession session = engine.getSession();
        myAppData = ByteBuffer.allocate(1024);
        myNetData = ByteBuffer.allocate(session.getPacketBufferSize());
        peerAppData = ByteBuffer.allocate(1024);
        peerNetData = ByteBuffer.allocate(session.getPacketBufferSize());

        peerAppData.clear();
		peerNetData.clear();
		myAppData.clear();
		myNetData.clear();

		peerNetData.flip();

		try{
			engine.beginHandshake();
		}
		catch(SSLException e){
			Logger.error("ssl excecption :(");
			return;
		}
		runHandshake();
	}

	public void failed(Throwable thr, NeedUnwrapRead obj){
		close();
	}

	public void completed(Integer read, NeedUnwrapRead obj){
		peerNetData.flip();
		if(read < 0){
			close();
		}
		else{
			iPeer.addTask(() -> {runHandshake();});
		}
	}


	protected void runHandshake(){

        SSLEngineResult result;
        HandshakeStatus handshakeStatus;

        while (true) {
			handshakeStatus = engine.getHandshakeStatus();

            switch (handshakeStatus) {
                case NEED_UNWRAP:

					try {
						result = engine.unwrap(peerNetData, peerAppData);
					} catch (SSLException sslException) {
						peerNetData.compact();
						if(!engine.isOutboundDone() && !engine.isInboundDone())
							Logger.error("Got SSL Exception " + sslException.toString());
						close();
						return;
					}

					switch (result.getStatus()) {
						case OK:
							peerNetData.compact();
							peerNetData.flip();
							break;
						case BUFFER_OVERFLOW:
							peerAppData = ByteBuffer.allocate(engine.getSession().getApplicationBufferSize());
							break;
						case BUFFER_UNDERFLOW:
							peerNetData.compact();
							socketChannel.read(peerNetData, new NeedUnwrapRead(), this);
							return;
						case CLOSED:
							peerNetData.compact();
							Logger.error(handshakePrepend + "Unwrap closed");
							close();
							return;
						default:
							throw new IllegalStateException("Invalid SSL status: " + result.getStatus());
					}
					break;
                case NEED_WRAP:
					Logger.debug(handshakePrepend + "Wrapping data to " + getPeerAddress());
					myNetData.clear();
                    try {
                        result = engine.wrap(myAppData, myNetData);
                    } catch (SSLException sslException) {
						close();
                        break;
                    }


					CompletionHandler<Integer, Object> inlined = new CompletionHandler<Integer, Object>() {
							@Override
							public void completed(Integer result, Object obj) {
								myNetData.compact();
								iPeer.addTask(() -> {runHandshake();});
							}

							@Override
							public void failed(Throwable exc, Object obj) {
								close();
							}
						};

                    switch (result.getStatus()) {
                        case BUFFER_OVERFLOW:
							Logger.debug(handshakePrepend + "got overflow");
							myNetData.clear();
							break;
                        case OK :
                            myNetData.flip();
							socketChannel.write(myNetData, null, inlined);
							return;
                        case BUFFER_UNDERFLOW:
							System.exit(5);
							break;
                        case CLOSED:
							Logger.debug(handshakePrepend + "Wrapped closed");
							try {
								myNetData.clear();
								peerNetData.clear();
								close();
							} catch (Exception e) {
							}
                            break;
                        default:
                            throw new IllegalStateException("Invalid SSL status: " + result.getStatus());
                    }
                    break;
                case NEED_TASK:
					Logger.debug("Handhshake: Running tasks");
                    Runnable task;
                    while ((task = engine.getDelegatedTask()) != null) {
						task.run();
                    }
                    break;
                case FINISHED:
					Logger.debug("Handshake finished");
                    break;
                case NOT_HANDSHAKING:
					Logger.debug("Not handshaking!");
                    break;
                default:
                    throw new IllegalStateException("Invalid SSL status: " + handshakeStatus);
            }

			handshakeStatus = engine.getHandshakeStatus();
			if(handshakeStatus == SSLEngineResult.HandshakeStatus.FINISHED || handshakeStatus == SSLEngineResult.HandshakeStatus.NOT_HANDSHAKING)
				break;
        }

		if(runOnHandShake != null){
			Runnable r = runOnHandShake;
			runOnHandShake = null;
			Logger.debug("runHandshake finished gracefully");
			r.run();
		}

	}

	private String getPeerAddress(){
		return peerAddress;
	}

	public void close(){

		//Logger.info(Arrays.toString(Thread.currentThread().getStackTrace()).replace( ',', '\n' ));

		if (!engine.isOutboundDone())
		{
			Logger.debug("Closing outbound with " + getPeerAddress());
			engine.closeOutbound();
			handshakePrepend = "CLOSE_NOTIFY: ";
			runHandshake();
		}
		else if (!engine.isInboundDone())
		{
			Logger.debug("Closing inbound with " + getPeerAddress());
			peerAppData.clear();

			try{
				engine.closeInbound();
			}catch(Exception e){}
			try{
				socketChannel.close();
				Logger.debug("Socket closed with " + getPeerAddress());
			}catch(IOException e){}
		}
		else if(engine.isInboundDone() && engine.isInboundDone()){
			try{
				socketChannel.close();
			}catch(IOException e){}
		}
	}

    protected KeyManager[] createKeyManagers(InputStream keyStoreIS, String keystorePassword, String keyPassword) throws KeyStoreException, NoSuchAlgorithmException, IOException, CertificateException, UnrecoverableKeyException {
        KeyStore keyStore = KeyStore.getInstance("JKS");
        try {
            keyStore.load(keyStoreIS, keystorePassword.toCharArray());
        } finally {
            if (keyStoreIS != null) {
                keyStoreIS.close();
            }
        }
        KeyManagerFactory kmf = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
        kmf.init(keyStore, keyPassword.toCharArray());
        return kmf.getKeyManagers();
    }

    protected TrustManager[] createTrustManagers(InputStream trustStoreIS, String keystorePassword) throws KeyStoreException, NoSuchAlgorithmException,IOException, CertificateException, UnrecoverableKeyException{
        KeyStore trustStore = KeyStore.getInstance("JKS");
        try {
            trustStore.load(trustStoreIS, keystorePassword.toCharArray());
        } finally {
            if (trustStoreIS != null) {
                trustStoreIS.close();
            }
        }
        TrustManagerFactory trustFactory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
        trustFactory.init(trustStore);
        return trustFactory.getTrustManagers();
    }



	public void write(ByteBuffer contents, Runnable runOnSuccess, Runnable runOnFailure){

			SSLEngineResult result;
			CompletionHandler<Integer, Boolean> inlined = new CompletionHandler<Integer, Boolean>() {
					@Override
					public void completed(Integer result, Boolean obj) {
						if(result < 0){
							close();
							runOnFailure.run();
						}
						else{

							if(obj)
								write(contents, runOnSuccess, runOnFailure);
							else
								runOnSuccess.run();
						}
					}

					@Override
					public void failed(Throwable exc, Boolean obj) {
						close();
						runOnFailure.run();
					}
				};


				myNetData.clear();
				try {
					result = engine.wrap(contents, myNetData);
				} catch (SSLException sslException) {
					Logger.debug("Write: got ssl exception");
					close();
					return;
				}

				switch (result.getStatus()) {
					case BUFFER_OVERFLOW:
						Logger.debug("got overflow");
						write(contents, runOnSuccess, runOnFailure);
						break;
					case OK :
						Logger.debug("Write: sending data");
						myNetData.flip();
						socketChannel.write(myNetData, contents.hasRemaining(), inlined);
						break;
					case BUFFER_UNDERFLOW:
						Logger.debug("Shouldn't have gotten here. No biggie though");
						break;
					case CLOSED:
						Logger.debug("Write: Closed on wrap");
						runOnFailure.run();
						try {
							myNetData.clear();
							peerNetData.clear();
							close();
						} catch (Exception e) {
						}
						return;
					default:
						throw new IllegalStateException("Invalid SSL status: " + result.getStatus());
				}

	}

	public void read(ResizableBuffer contents, Runnable runOnSuccess, Runnable runOnFailure){
			
			SSLEngineResult result;

			try {
				result = engine.unwrap(peerNetData, contents.getBuffer());
			} catch (SSLException sslException) {
				Logger.debug("Read: Got SSL Exception " + sslException.toString());
				runOnFailure.run();
				close();
				return;
			}

			CompletionHandler<Integer, Object> readInline = new CompletionHandler<Integer, Object>(){
					@Override
					public void completed(Integer result, Object obj) {
						peerNetData.flip();

						if(result < 0){
							runOnFailure.run();
							close();
						}
						else{

							iPeer.addTask(()-> {read(contents, runOnSuccess, runOnFailure);});
						}
					}

					@Override
					public void failed(Throwable exc, Object obj) {
						Logger.debug("Failed reading from " + getPeerAddress());
						close();
						runOnFailure.run();
					}
			};

			switch (result.getStatus()) {
				case OK:
					peerNetData.compact();
					peerNetData.flip();
					runOnSuccess.run();
					break;
				case BUFFER_OVERFLOW:
					contents.resize();
					iPeer.addTask(()-> {read(contents, runOnSuccess, runOnFailure);});
					break;
				case BUFFER_UNDERFLOW:
					peerNetData.compact();
					socketChannel.read(peerNetData, null, readInline);
					return;
				case CLOSED:
					peerNetData.compact();
					Logger.debug("Read: Unwrap closed with " + getPeerAddress());
					runOnFailure.run();
					close();
					break;
				default:
					throw new IllegalStateException("Invalid SSL status: " + result.getStatus());
			}

	}

	public boolean isOver(){
		if(engine == null)
			return true;
		return engine.isOutboundDone() || engine.isInboundDone();
	}

}
