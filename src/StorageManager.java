import java.nio.channels.AsynchronousFileChannel;
import java.nio.channels.CompletionHandler;
import java.nio.ByteBuffer;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicInteger;
import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.util.function.Function;
import java.io.Serializable;

public class StorageManager implements Serializable{

	private long maxSpace;
	private transient long usedSpace = 0;
	private ConcurrentHashMap<String, ConcurrentHashMap<Long, ChunkData>> idToChunks; 
	private transient ConcurrentHashMap<String, ChunkData> allChunks = new ConcurrentHashMap<String, ChunkData>(); 
	private transient ConcurrentHashMap<String, ChunkGrant> chunksProcessing = new ConcurrentHashMap<String, ChunkGrant>(); 
	private IPathProvider pathProvider;
	private transient Function<ChunkData, Void> onChunkStoredRemove;
	private ConcurrentHashMap<String, ConcurrentHashMap<Long, ChunkData>> deletedFiles = new ConcurrentHashMap<String, ConcurrentHashMap<Long, ChunkData>>(); 

	public StorageManager(long maxSpace, IPathProvider pathProvider, Function<ChunkData, Void> onChunkStoredRemove){
		this.maxSpace = maxSpace;
		idToChunks = new ConcurrentHashMap<String, ConcurrentHashMap<Long, ChunkData>>();
		this.pathProvider = pathProvider;
		this.onChunkStoredRemove = onChunkStoredRemove;
	}

	private class ChunkGrant implements Runnable{
		private ChunkData chData;
		private AtomicInteger runTimes = new AtomicInteger(1);
		private Runnable r;
		public ChunkGrant(ChunkData chData, Runnable r){
			this.chData = chData;
			this.r = r;
		}

		public ChunkData getChunk(){
			return chData;
		}

		public void incrementRunTimes(){
			runTimes.getAndIncrement();
		}

		public void run(){
			while(runTimes.getAndDecrement() != 0){
				r.run();
			}
		}
	}


	private synchronized ChunkGrant acquireProcessingRights(ChunkData chData, Runnable r) throws ChunkTooBig{
		final ChunkGrant grant = new ChunkGrant(chData, r);
		ChunkGrant cur = chunksProcessing.computeIfAbsent(chData.getUniqueIdentifier(), (k) -> {return grant;});
		if(cur != grant){
			Logger.info("Chunk already being processed, will increment run times " + chData.getUniqueIdentifier());
			cur.incrementRunTimes();
			return null;
		}
		if(!requestGrantSpace(grant)){
			Logger.warn("Chunk too big to be stored " + grant.getChunk().getUniqueIdentifier() + " " + grant.getChunk().getSize());
			chunksProcessing.remove(chData.getUniqueIdentifier());
			throw new ChunkTooBig();
		}
		return grant;
	}

	private synchronized boolean requestGrantSpace(ChunkGrant grant){
		int spaceNeeded = grant.getChunk().getSize();
		if(usedSpace + spaceNeeded > maxSpace)
			return false;

		usedSpace+=spaceNeeded;
		return true;
	}

	private synchronized void grantProcessSuccess(ChunkGrant grant){
		ConcurrentHashMap<Long, ChunkData> chunkMap = idToChunks.computeIfAbsent(grant.getChunk().getFileId(), (k) -> {return new ConcurrentHashMap<Long, ChunkData>();});
		allChunks.putIfAbsent(grant.getChunk().getUniqueIdentifier(), grant.getChunk());

		ChunkData res = chunkMap.computeIfAbsent(grant.getChunk().getChunkNo(), (k) -> {return grant.getChunk();});
		if(res != grant.getChunk())
			Logger.warn("StorageManager is in an unstable state, please debug");

		grant.run();
		String fileId = grant.getChunk().getFileId();
		ConcurrentHashMap<Long, ChunkData> removedChunks = deletedFiles.get(fileId);
		if(removedChunks != null){
			removedChunks.remove(grant.getChunk().getChunkNo());

			if(removedChunks.size() == 0)
				deletedFiles.remove(fileId);
		}
		releaseGrant(grant, false);
	}

	private synchronized void releaseChunkSpace(ChunkData chData, boolean stored){
		releaseChunkSpace(chData, stored, onChunkStoredRemove);
	}

	private synchronized void releaseChunkSpace(ChunkData chData, boolean stored, Function<ChunkData, Void> runOnRemove){
		allChunks.remove(chData.getUniqueIdentifier());
		usedSpace -= chData.getSize();

		if(!stored)
			return;

		ConcurrentHashMap<Long, ChunkData> hMap = idToChunks.get(chData.getFileId());
		if(hMap != null)
			hMap.remove(chData.getChunkNo());

		runOnRemove.apply(chData);

		ConcurrentHashMap<Long, ChunkData> removedChunks = deletedFiles.computeIfAbsent(chData.getFileId(), (k) -> {return new ConcurrentHashMap<Long, ChunkData>();});
		removedChunks.putIfAbsent(chData.getChunkNo(), chData);


		File file = new File(pathProvider.chunkToPath(chData));
		try{
			file.delete();
		}
		catch(SecurityException e){
		}

	}

	private synchronized void releaseGrant(ChunkGrant grant, boolean freeSpace){
		int spaceNeeded = grant.getChunk().getSize();

		//Sanity check on grant space removal
		if(freeSpace && chunksProcessing.get(grant.getChunk().getUniqueIdentifier())==grant)
			releaseChunkSpace(grant.getChunk(), false);
		chunksProcessing.remove(grant.getChunk().getUniqueIdentifier());
	}

	private synchronized void releaseProcessingRights(ChunkData chData){
		chunksProcessing.remove(chData.getUniqueIdentifier());
	}

	public synchronized void storeChunk(ChunkData chData, byte[] body, Runnable runOnSuccess, Runnable runOnRunningOrFail, Runnable runOnAlreadyStored){

		Logger.info("Will try to store " + chData);
		ConcurrentHashMap chunkMap = idToChunks.computeIfAbsent(chData.getFileId(), (k) -> {return new ConcurrentHashMap<Long, ChunkData>();});


		if(chunkMap.get(chData.getChunkNo()) == null){

			ChunkGrant ogGrant = null;
			try{
				ogGrant = acquireProcessingRights(chData, runOnSuccess);
			}
			catch(ChunkTooBig e){
				runOnRunningOrFail.run();
				return;
			}

			final ChunkGrant grant = ogGrant;
			if(grant == null){
				return;
			}


			try{
				AsynchronousFileChannel fileChannel = AsynchronousFileChannel.open(Paths.get(pathProvider.chunkToPath(grant.getChunk())), StandardOpenOption.WRITE, StandardOpenOption.TRUNCATE_EXISTING, StandardOpenOption.CREATE);
				ByteBuffer buf = ByteBuffer.wrap(body);
				fileChannel.write(buf, 0, buf,
					new CompletionHandler<Integer, ByteBuffer>() {
						public void completed(Integer result, ByteBuffer attachment) {
							grantProcessSuccess(grant);
							try{
								fileChannel.close();
							}
							catch(IOException e){}

						}

						public void failed(Throwable exception, ByteBuffer attachment) {
							Logger.error("Write chunk on STORE failed");
							runOnRunningOrFail.run();
							releaseGrant(grant, true);
							try{
								fileChannel.close();
							}
							catch(IOException e){}
						}
					}
				);
			}
			catch(IOException e){
				Logger.error("IOException on writing chunk");
				runOnRunningOrFail.run();
				e.printStackTrace();
				releaseGrant(grant, true);
				return;
			}
		}
		else{
			runOnAlreadyStored.run();
		}

	}

	public boolean chunkExist(ChunkData chData){
		return (allChunks.get(chData.getUniqueIdentifier()) != null);
	}

	public synchronized void getPhysicalChunk(ChunkData chData, Function<ByteBuffer, Void> runOnSuccess, Runnable runAlways){

		ChunkData cur = allChunks.get(chData.getUniqueIdentifier());
		if(cur == null)
			return;

		try{
			AsynchronousFileChannel fileChannel = AsynchronousFileChannel.open(Paths.get(pathProvider.chunkToPath(cur)), StandardOpenOption.READ);

			ByteBuffer buf = ByteBuffer.allocate(cur.getSize());
			fileChannel.read(buf, 0, buf,
				new CompletionHandler<Integer, ByteBuffer>() {
					public void completed(Integer result, ByteBuffer attachment) {
						runOnSuccess.apply(attachment);
						runAlways.run();
						try{
							fileChannel.close();
						}
						catch(IOException e){}
					}

					public void failed(Throwable exception, ByteBuffer attachment) {
						Logger.error("Could not read chunk " + chData.getUniqueIdentifier());
						runAlways.run();
						try{
							fileChannel.close();
						}
						catch(IOException e){}
					}
				}
			);
		}
		catch(IOException e){
			Logger.error("IOException on read chunk " + chData.getUniqueIdentifier());
			e.printStackTrace();
			runAlways.run();
			return;
		}
	}


	public synchronized void deleteChunk(ChunkData chData){
		deleteChunk(chData, onChunkStoredRemove);
	}

	public synchronized void deleteChunk(ChunkData chData, Function<ChunkData, Void> onRemove){
		ChunkData res = allChunks.get(chData.getUniqueIdentifier());
		if(res == null)
			return;
		File file = new File(pathProvider.chunkToPath(res));
		try{
			releaseChunkSpace(res, true, onRemove);
			file.delete();
		}
		catch(SecurityException e){
		}

	}

	public synchronized void deleteFile(String fileId){
		deleteFile(fileId, onChunkStoredRemove);
	}	

	public synchronized void deleteFile(String fileId, Function<ChunkData, Void> onRemoved){

		ConcurrentHashMap<Long, ChunkData> removedChunks = deletedFiles.get(fileId);
		if(removedChunks != null){
			removedChunks.forEach((k,v) -> { onRemoved.apply(v); });
		}
		
		ConcurrentHashMap<Long, ChunkData> hMap = idToChunks.get(fileId);
		if(hMap == null)
			return;
		hMap.forEach( (k,v) -> {
			File file = new File(pathProvider.chunkToPath(v));
			try{
				releaseChunkSpace(v, true, onRemoved);
				file.delete();
			}
			catch(SecurityException e){
			}
		});
		//Must be done after since releaseChunkSpace depends on it
		idToChunks.remove(fileId);

	}

	public synchronized void  getChunk(ChunkData chData, Function<ChunkData, Void> func){
		ChunkData cur = allChunks.get(chData.getUniqueIdentifier());
		if(cur == null)
			return;
		func.apply(cur);
	}


	public synchronized void reclaimSpace(long newMaxSpace){

		this.maxSpace = newMaxSpace;

		//Special case
		if(maxSpace == 0){
			allChunks.forEach( (k,v) -> { releaseChunkSpace(v, true) ;});
			return;
		}

		if(usedSpace > maxSpace){

			ChunkData[] chunks = allChunks.values().stream()
								.sorted((l, r) -> {
									return (r.getApparentReplicationDegree()-r.getDesiredReplicationDegree()) -
									(l.getApparentReplicationDegree() - l.getDesiredReplicationDegree()) ;
								})
								.toArray(ChunkData[]::new);
			for(int i=0;i<chunks.length && usedSpace > maxSpace; i++){
				releaseChunkSpace(chunks[i], true);
			}

		}
	}

	public synchronized long runOnAllChunks(Function<ChunkData, Void> func){
		allChunks.forEach(
				(k,v) ->
				{
					func.apply(v);
				});
		return maxSpace;
	}

	public synchronized void regenerate(Function<ChunkData, Void> onChunkStoredRemove, long maxSize){

		if(maxSize < maxSpace)
			Logger.warn("STORAGE: New max size is smaller("+maxSize+" vs " + maxSpace + "). If it's not a typo please run RECLAIM for confirmation");
		else
			maxSpace = maxSize;

		this.onChunkStoredRemove = onChunkStoredRemove;
		allChunks = new ConcurrentHashMap<String, ChunkData>(); 
		chunksProcessing = new ConcurrentHashMap<String, ChunkGrant>(); 

		AtomicLong counter = new AtomicLong(0);
		idToChunks.forEach(
				(k,v) ->
				{
					v.forEach(
							(ignore, chunk) ->
								{
									allChunks.putIfAbsent(chunk.getUniqueIdentifier(), chunk);
									counter.getAndAdd(chunk.getSize());
								}
							);
				});

		usedSpace = counter.get();

	}

}
