public enum MessageType {
        FIND_SUCCESSOR,   // lookup key
        PREDECESSOR,  //get nodes predecessor
		NOTIFY,
        KEY,  // get nodes key
        PUT, // store chunk
        OK, // Any acknowledges
        PUTCHUNK,
        STORED,
        GETCHUNK,
        DELETE,
		REMOVED,
		CHUNK,
		REMOVED_DELETE,
		BEG,
		BEG_REMOVED,
        YEET,
        YEETED
}
