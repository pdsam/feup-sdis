import java.nio.ByteBuffer;
import java.nio.Buffer;

public class ResizableBuffer{

	private ByteBuffer buf;

	public ResizableBuffer(){
		this.buf = ByteBuffer.allocate(2048);
	}

	public ResizableBuffer(ByteBuffer buf){
		this.buf = buf;
	}

	public ResizableBuffer(int size){
		this.buf = ByteBuffer.allocate(size);
	}

	public Buffer flip(){
		return buf.flip();
	}

	public Buffer clear(){
		return buf.clear();
	}

	public Buffer compact(){
		return buf.compact();
	}
	
	public void resize(){
		buf.flip();
		ByteBuffer newBuf = ByteBuffer.allocate(buf.capacity()*2);
		newBuf.put(buf);
		this.buf = newBuf;
	}

	public ByteBuffer getBuffer(){
		return buf;
	}

	public byte[] array(){
		return buf.array();
	}

	public int position(){
		return buf.position();
	}

	public String toString(){
		return "ResizableString " + buf;
	}

	public int capacity(){
		return buf.capacity();
	}
}
