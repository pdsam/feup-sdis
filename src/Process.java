public interface Process extends Runnable {
	public void waitTermination();
	public void stop(boolean kill);
	public int returnStatus();
	public boolean isStopped();
}

