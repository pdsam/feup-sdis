import java.rmi.RemoteException;
import java.util.concurrent.ConcurrentHashMap;
import java.nio.ByteBuffer;
import java.io.ByteArrayOutputStream;
import java.io.ObjectOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ObjectInputStream;
import java.io.IOException;
import java.util.function.Function;
import java.io.EOFException;


public class PeerServerHandler{

	ChordPeer iPeer;
	int counter = 0;

	public PeerServerHandler(ChordPeer iPeer){
		this.iPeer = iPeer;
	}


	private class PeerConnectionProcess implements Process{
		int id;
		SSLConnectionManager manager;
		ResizableBuffer buf = new ResizableBuffer();
		boolean isStopped = false;

		public PeerConnectionProcess(int id, SSLConnectionManager manager){
			this.id = id;
			this.manager = manager;
		}

		public int getId(){
			return id;
		}

		public void waitTermination(){}
		public void stop(boolean kill){
			if(isStopped)
				return;
			manager.close();
			PeerServerHandler.this.programs.remove(id);
		}

		public int returnStatus(){return 0;}

		private void sendObject(Object o, Runnable runOnSuccess, Runnable runOnFailure){

				try{
					ByteArrayOutputStream out = new ByteArrayOutputStream();
					ObjectOutputStream os = new ObjectOutputStream(out);
					os.writeObject(o);
					manager.write(ByteBuffer.wrap(out.toByteArray()), runOnSuccess, runOnFailure);	
				}
				catch(IOException e){
					Logger.error("Problem serializing object");
					stop(true);
				}
		}

		public void readLoop(){
			Message msg = null;
			//Logger.info("I read " + buf.toString());

			try{
				ByteArrayInputStream in = new ByteArrayInputStream(buf.array(), 0, buf.position());
				ObjectInputStream is = new ObjectInputStream(in);
				msg = (Message)is.readObject();
			}
			catch(EOFException e){
				//object is bigger will read more :)
				manager.read(buf, ()->{readLoop();}, ()->{stop(true);});
			}
			catch(IOException e){
				Logger.error("Problem de-serializing object " + buf);
				stop(true);
				return;
			}
			catch(ClassNotFoundException e){
				Logger.error("Could not find class for de-serialization");
				stop(true);
			}
			//Logger.info("Message is " + msg.getType().name());

			switch(msg.getType()){
				case FIND_SUCCESSOR:

					Function<Finger, Void> r = (finger)->
					{
						Logger.info("Sending FIND_SUCESSOR: " + finger);
						sendObject(finger, ()->{Logger.success("Sent Finger back!");}, ()->{Logger.error("Failed to send Finger!");});
						return null;
					};
					PeerServerHandler.this.iPeer.find_successor((String)msg.getBody(), r);
					break;
				case PREDECESSOR:
					sendObject(PeerServerHandler.this.iPeer.getPredecessor(), ()->{}, ()->{});
					break;
				case NOTIFY:
					PeerServerHandler.this.iPeer.notify((Finger)msg.getBody());
					break;
				case PUTCHUNK:
					PutChunkPayload payload = (PutChunkPayload)msg.getBody();

					if(payload.getOwnerId().equals(PeerServerHandler.this.iPeer.getId())){
						Logger.info("Got my own PUTCHUNK for " + payload + " will bounce");

						byte[] bouncer = new byte[buf.position()];
						System.arraycopy(buf.array(), 0, bouncer, 0, bouncer.length);
						manager.close();

						Finger nearestFinger = PeerServerHandler.this.iPeer.findEarliestFreeFinger();
						if(nearestFinger == null){
							Logger.info("No finger to bounce my own putchunk " + payload);
						}
						else{
							PeerServerHandler.this.iPeer.connectToPeer(nearestFinger.getAddress(),
									(newManager)->{
										newManager.write(ByteBuffer.wrap(bouncer), manager::close, ()->{});	
										return null;
							});
						}
						
					}
					else
						PeerServerHandler.this.iPeer.handlePutChunk(payload);
					break;
				case YEET:
					break;
				case STORED:
					PeerServerHandler.this.iPeer.stored(manager, (StoredPayload)msg.getBody());
					break;
				case GETCHUNK:
					PeerServerHandler.this.iPeer.getchunk((object)->{sendObject(object, ()->{}, ()->{}); return null;}, (ChunkData)msg.getBody());
					break;
				case REMOVED:
					RemovedPayload removed = (RemovedPayload)msg.getBody();
					PeerServerHandler.this.iPeer.removed(removed);
					stop(false);
					break;
				case DELETE:
					PeerServerHandler.this.iPeer.deleteChunks((DeletePayload)msg.getBody(), (chunk)->{
						sendObject(new Message(MessageType.REMOVED_DELETE, chunk), ()->{}, ()->{manager.close();
						});
						return null;	
					});
					stop(false);
					return;
				case BEG:
					if(!PeerServerHandler.this.iPeer.gotBegged((ChunkData)msg.getBody())){
						sendObject(new Message(MessageType.BEG_REMOVED, null), ()->{stop(false);}, ()->{});
					}
					else
						stop(false);
					return;
				default:
					throw new RuntimeException("Not implemented " + msg.getType().name());
			}
			buf.clear();
			manager.read(buf, ()->{readLoop();}, ()->{stop(true);});
		}

		public void run(){
			manager.setRunOnHandshake(()->{
				manager.read(buf, ()->{readLoop();}, ()->{stop(true);});
			});
			manager.runConnection();
		}

		public boolean isStopped(){
			return isStopped;
		}


	}

	ConcurrentHashMap<Integer, PeerConnectionProcess> programs = new ConcurrentHashMap<Integer, PeerConnectionProcess>();

	public synchronized void addConnection(SSLConnectionManager sslManager){
		PeerConnectionProcess program = new PeerConnectionProcess(counter++, sslManager);
		programs.put(program.getId(), program);
		iPeer.addTask(program);
	}

}
