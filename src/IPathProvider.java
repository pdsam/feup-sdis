public interface IPathProvider{
	public String chunkToPath(ChunkData chData);
	public String restoreDir();
}

