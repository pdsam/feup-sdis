import java.io.PrintStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Logger {
    private static PrintStream logFile = System.out;
	private static boolean allowDebug = true;

    private static DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss.SSS");


    private static String getHeader() {
        Date date = new Date();
        return "" + Thread.currentThread().getId() + ", " + dateFormat.format(date) + ": ";
    }

    synchronized public static void success(String msg) {
        logFile.println("SUCCESS: " + getHeader() + msg);
        logFile.flush();
    }
    synchronized public static void error(String msg) {
        logFile.println("ERROR: " + getHeader() + msg);
        logFile.flush();
    }

    synchronized public static void warn(String msg) {
        logFile.println("WARN:  " + getHeader() + msg);
        logFile.flush();
    }

    synchronized public static void info(String msg) {
        logFile.println("INFO:  " + getHeader() + msg);
        logFile.flush();
    }

    synchronized public static void debug(String msg) {
		if(!allowDebug)
			return;
        logFile.println("DEBUG:  " + getHeader() + msg);
        logFile.flush();
    }

	public static void setDebug(boolean val){
		allowDebug = val;
	}
}
