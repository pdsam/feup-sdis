import java.io.Serializable;

public class ChunkPayload implements Serializable{

	byte[] buffer;

	public ChunkPayload(byte[] buffer){
		this.buffer = buffer;
	}

	public byte[] getBuffer(){
		return buffer;
	}
}
