import java.rmi.RemoteException;
import java.util.concurrent.Future;

public interface IPeer{

	void addTask(Runnable r);
	Future addTaskWithDelay(Runnable r, long delay);
	String restoreDir();
	void deleteLocal(String fileId);
}
