import java.io.*;
import java.net.InetSocketAddress;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicReferenceArray;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousServerSocketChannel;
import java.nio.channels.AsynchronousSocketChannel;
import java.nio.channels.AsynchronousChannelGroup;
import java.nio.channels.CompletionHandler;

import java.util.function.Function;

import java.util.concurrent.Future;
import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;

public class Peer implements ChordPeer, CompletionHandler<AsynchronousSocketChannel, Void>, ChordRemote, IPathProvider, Serializable{

    
    private transient Finger predecessor;
	private transient Finger ownFinger;
    private transient AtomicReferenceArray<Finger> fingers;
    private transient ScheduledThreadPoolExecutor tPool;
	private transient AsynchronousServerSocketChannel listener;
	private transient PeerServerHandler serverHandler;
	private transient int next = 0;

	private ConcurrentHashMap<String, String> nameToId = new ConcurrentHashMap<String, String>();
	private ConcurrentHashMap<String, BackedupFile> idToFile = new ConcurrentHashMap<String, BackedupFile>();
	private StorageManager storageManager;

	private transient PutChunkManager putChunkManager = new PutChunkManager(this);
	private transient RestoreManager restoreManager = new RestoreManager(this);
	private transient DeleteManager deleteManager = new DeleteManager(this);


    public Peer(String id, Long sizeInMb, String ip, Integer port) throws IOException{
		regenerate(true, id, sizeInMb, ip, port);
	}

	public void regenerate(boolean creation, String id, Long sizeInMb, String ip, Integer port) throws IOException{
		ownFinger = new Finger(id, new InetSocketAddress(ip, port));
		fingers = new AtomicReferenceArray<>(Constants.M +1);
		tPool = new ScheduledThreadPoolExecutor(Runtime.getRuntime().availableProcessors());
		next = 0;
		putChunkManager = new PutChunkManager(this);
		restoreManager = new RestoreManager(this);
		deleteManager = new DeleteManager(this);
		serverHandler = new PeerServerHandler(this);

		tPool.scheduleWithFixedDelay(deleteManager, 0, 10000, TimeUnit.MILLISECONDS);
		tPool.scheduleWithFixedDelay(()->{beggar();}, 5000, 10000, TimeUnit.MILLISECONDS);

		listener = AsynchronousServerSocketChannel.open(AsynchronousChannelGroup.withFixedThreadPool(2, Executors.defaultThreadFactory())).bind(new InetSocketAddress(ip, port));


		Function<ChunkData, Void> removeFunction =
				(chunkData)->{

					find_successor(chunkData.getSenderId(),


							(finger)->{

								if(!finger.getId().equals(chunkData.getSenderId())){
									Logger.warn("Cant find original peer " + chunkData.getSenderId() + " got " + finger);
									return null;
								}

								connectToPeer(finger.getAddress(),
										(connection) ->
										{

											Message message = new Message(MessageType.REMOVED, new RemovedPayload(chunkData, getId()));
											ByteArrayOutputStream out = new ByteArrayOutputStream();
											try {
												ObjectOutputStream os = new ObjectOutputStream(out);
												os.writeObject(message);
											} catch (IOException e) {
												e.printStackTrace();
												connection.close();
												return null;
											}
											connection.write(ByteBuffer.wrap(out.toByteArray()), connection::close, ()->{});
											return null;
										}
										);
								return null;
									}
							);


					return null;
		};


		if(creation)
			storageManager = new StorageManager(sizeInMb*Constants.CHUNK_MAX_SIZE_BYTES, this, removeFunction);
		else
			storageManager.regenerate(removeFunction, sizeInMb*Constants.CHUNK_MAX_SIZE_BYTES);
		File file = new File(chunksDir());
		if(file.exists()){
			if(!file.isDirectory())
				throw new IOException("Cant create folder");
		}
		else
			file.mkdirs();
	}

	public void beggar(){
		Logger.info("Running beggar");
		idToFile.forEach( (k,v)->{
			if(putChunkManager.processExists(k)){
				return;
			}
			else{

				ArrayList<Long> chunks = v.chunksWithoutDesiredReplication();
				if(chunks.size() == 0)
					return;

				Logger.info("BEGGAR: Chunks missing for: " + k);
				Random rand = new Random();
				long chunkNo = chunks.get(rand.nextInt(chunks.size()));

				ArrayList<String> replicators = v.getReplicatorsForChunk(chunkNo);
				if(replicators.size() == 0)
					return;

				String peerWithChunk = replicators.get(rand.nextInt(replicators.size()));
				Logger.info("BEGGAR: Chose " + peerWithChunk + " to replicate");

				Runnable handler = ()->
				{
					find_successor(peerWithChunk,
							(finger)->
							{
								if(!finger.getId().equals(peerWithChunk)){
									Logger.warn("BEGGAR: Could not find original peerWithChunk " + peerWithChunk);
									return null;
								}


								connectToPeer(finger.getAddress(),

										(connection)->
										{

											ResizableBuffer buf = new ResizableBuffer();
											Runnable runOnSuccess = ()->{
												Message msg = null;
												try{
													ByteArrayInputStream in = new ByteArrayInputStream(buf.array(), 0, buf.position());
													ObjectInputStream is = new ObjectInputStream(in);
													msg = (Message)is.readObject();
												}
												catch(IOException e){
													Logger.error("RESTORE: Problem de-serializing object " + buf);
													return;
												}
												catch(ClassNotFoundException e){
													Logger.error("RESTORE: Could not find class for de-serialization");
													return;
												}

												if(msg.getType() != MessageType.BEG_REMOVED){
													Logger.warn("BEGGAR: Got invalid response for BEG: " + msg.getType().name());
													return;
												}

												v.removeChunkStored(new ChunkData(peerWithChunk, k, chunkNo, 0, 0));
												connection.close();
											};

											try{
												final String fileId = k;
												Message msg = new Message(MessageType.BEG, v.getChunkData(chunkNo));
												ByteArrayOutputStream out = new ByteArrayOutputStream();
												ObjectOutputStream os = new ObjectOutputStream(out);
												os.writeObject(msg);
												connection.write(ByteBuffer.wrap(out.toByteArray()), 
														()->{
															connection.read(buf,
																	runOnSuccess,
																	()->{});

														}
														, ()->{});	
											}
											catch(IOException e){
												Logger.error("Problem serializing object");
											}

											return null;
										}
										);
								return null;
							}
							);

				};


				addTask(handler);

			}

		});

	}

	@Override
	public void delete(String filePath) throws RemoteException{
		File file = new File(filePath);
		String id = nameToId.get(file.getAbsolutePath());
		if(id == null){
			throw new RemoteException("File does not exist");
		}

		Logger.info("Sending delete for " +filePath);
		nameToId.remove(file.getAbsolutePath());

		BackedupFile backedup = idToFile.get(id);
		if(backedup == null)
			return;
		idToFile.remove(id);
		deleteManager.deleteFile(backedup);

	}
	@Override
	public void deleteLocal(String fileId){
    	Logger.debug("Deleting local copy of: "+fileId);
		storageManager.deleteFile(fileId);
	}

	public int restore(String filePath) throws RemoteException {

		File file = new File(filePath);

		String id = nameToId.get(file.getAbsolutePath());
		if(id == null)
			throw new RemoteException("File does not exist");
		
		Logger.info("Someone wants to RESTORE this file " + filePath);

		BackedupFile backedup = idToFile.get(id);
		if(backedup == null)
			throw new RemoteException("FIle was removed between calls, TOCTOU");

		Process process = restoreManager.addProcess(backedup);
		process.waitTermination();

		int returnStatus = process.returnStatus();
		if(returnStatus < 0){
			Logger.warn("RESTORE: Restoration of " + filePath + " failed!");
		}

		return returnStatus;
	}

	public int backup(String filePath, int desiredReplicationDegree) throws RemoteException{

		if(1 > desiredReplicationDegree || desiredReplicationDegree > Constants.M)
			throw new RemoteException("Replication degree must be in the 1-"+Constants.M+" range");

		File file = new File(filePath);
		if(!file.exists()){
			throw new RemoteException("File does not exist");
		}
		Logger.info("Someone wants to BACKUP this file " + filePath);

		int returnCode = 0;
		try{

			final BackedupFile backedup = new BackedupFile(file, getId(), desiredReplicationDegree);
			BackedupFile res = idToFile.computeIfAbsent(backedup.getId(), (k) -> { return backedup; });

			if(res != backedup){
				throw new RemoteException("File being BACKED UP or already BACKED UP");
			}

			deleteManager.fileBackedup(backedup);

			Process process = putChunkManager.createProcess(backedup);
			process.waitTermination();
			nameToId.putIfAbsent(backedup.getFilePath(), backedup.getId());

			returnCode = process.returnStatus();
			if(returnCode < 0){
				Logger.error("BACKUP: Failed of " + backedup.getFilePath() + ", issuing a delete for the file");
				this.addTask( () ->
						{
							//Required for delete
							try{
								this.delete(backedup.getFilePath());
							}
							catch(RemoteException e){}
						});

				return returnCode;
			}
			else
				Logger.success("BACKUP: Process of " + backedup.getFilePath() + " finished!");
			
		}
		catch(NoSuchAlgorithmException e){
			e.printStackTrace();
			throw new RemoteException("Could not get hash algorithm");
		}
		catch(FileTooBigException e){
			throw new RemoteException("File is too big to be backedup");
		}

		return returnCode;
	}

	@Override
	public void handlePutChunk(PutChunkPayload payload) {
		Logger.info("Got putchunk " + payload);
		Runnable runToSend = () -> {
			payload.decrementTTL();
			if(payload.getTTL() == 0)
				return;
			Finger nearestFinger = findEarliestFreeFinger();
			if(nearestFinger == null){
				Logger.warn("HANDLE-PUTCHUNK: No nearest finger to pass the payload");
				return;
			}
			Logger.info("HANDLE-PUTCHUNK: Sending " + payload + " to " + nearestFinger);
			connectToPeer(nearestFinger.getAddress(), connection -> {
				Message message = new Message(MessageType.PUTCHUNK, payload);
				ByteArrayOutputStream out = new ByteArrayOutputStream();
				try {
					ObjectOutputStream os = new ObjectOutputStream(out);
					os.writeObject(message);
				} catch (IOException e) {
					e.printStackTrace();
				}
				connection.write(ByteBuffer.wrap(out.toByteArray()), connection::close, ()->{});
				return null;
			});
		};

		Function<Boolean, Void> runOnStored = (shouldIncrement) -> {

			//find peer
			find_successor(payload.getOwnerId(), finger -> {

				if(!payload.getOwnerId().equals(finger.getId())){
					Logger.warn("HANDLE-PUTCHUNK: Couldn't find original node " + payload.getOwnerId() + " got " + finger.getId());
					return null;
				}
				// connect
				connectToPeer(finger.getAddress(), connection -> {

					//send STORED
					Message message = new Message(MessageType.STORED, new StoredPayload(getId(), payload.getFileId(), payload.getChunkNo()));
					ByteArrayOutputStream out = new ByteArrayOutputStream();
					try {
						ObjectOutputStream os = new ObjectOutputStream(out);
						os.writeObject(message);
					} catch (IOException e) {
						e.printStackTrace();
						connection.close();
						return null;
					}
					connection.write(ByteBuffer.wrap(out.toByteArray()), () -> awaitYeet(connection, new ResizableBuffer()), ()->{});
					return null;
				});
				return null;
			});

			if(shouldIncrement){
				payload.incrementApparentReplicationDegree();
				if(payload.getApparentReplicationDegree() >= payload.getDesiredReplicationDegree())
					return null;
			}
			runToSend.run();
			return null;
		};

		if(payload.getTTL() == 0){
			Logger.warn("Got CHUNk with TTL 0, please look into it");
			return;
		}
		if(payload.getApparentReplicationDegree() >= payload.getDesiredReplicationDegree()){
			Logger.warn("Some peer sent me a CHUNK even though has been fully replicated");
			runToSend.run();
			return;
		}

		ChunkData chData = new ChunkData(payload.getOwnerId(), payload.getFileId(), payload.getChunkNo(), payload.getDesiredReplicationDegree(), payload.getContent().length);
		storageManager.storeChunk(chData, payload.getContent(), ()->{runOnStored.apply(true);}, runToSend,
				()->{runOnStored.apply(false);});

	}



	public void run(String ip, Integer port){
		create();
		listener.accept(null, this);
		Logger.info("Listening for calls");

		if(ip != null && port != null){
			join(new InetSocketAddress(ip, port));
		}

		tPool.scheduleWithFixedDelay( ()->{stabilize();}, 4, 8, TimeUnit.SECONDS);
		tPool.scheduleWithFixedDelay( ()->{check_predecessor();}, 8, 8, TimeUnit.SECONDS);
		tPool.scheduleWithFixedDelay( ()->{fix_fingers();}, 2, 8, TimeUnit.SECONDS);
	}

	public void completed(AsynchronousSocketChannel ch, Void vd){
		Logger.debug("Successfully received a connection");

		SSLConnectionManager sslConn = new SSLConnectionManager(ch, false, this);

		serverHandler.addConnection(sslConn);
		listener.accept(null, this);
	}


	public void failed(Throwable th, Void vd){
		Logger.error("Failed to establish connection");
	}

	public static void main(String[] args){

		Logger.setDebug(false);
		//argument format
		//<USERNAME> <PASSWORD> <SIZE> <MY IP> <MY PORT> [<INITIATOR IP> <INITIATOR PORT>]

		if(args.length < 5){
			Logger.error("Arguments missing: <USERNAME> <PASSWORD> <SIZE> <MY IP> <MY PORT> [<INITIATOR IP> <INITIATOR PORT>");
			return;
		}

		if(args.length > 7){
			Logger.error("Too many arguments");
			return;
		}

		if(args.length != 5 && args.length != 7){
			Logger.info("Format is <USERNAME> <PASSWORD> <SIZE> <MY IP> <MY PORT> [<INITIATOR IP> <INITIATOR PORT>]");
			return;
		}

		String id = hashAlgorithm((args[0]+args[1]).getBytes());

		Peer peer = null;
		File file = new File(id+".peer");
		try{
			FileInputStream fileInput = new FileInputStream(file);
			ObjectInputStream objectInputStream = new ObjectInputStream(fileInput);
			Logger.info("Reading peer data from disk");
			try{
				peer = (Peer)objectInputStream.readObject();
				peer.regenerate(false, id, Long.valueOf(args[2]), args[3], Integer.valueOf(args[4]));
			}
			catch(ClassNotFoundException e){
				Logger.info("Class not found, leaving");
				return;
			}
		}
		catch(FileNotFoundException e){
			Logger.info("Creating brand new peer");
			try{
				peer = new Peer(id, Long.valueOf(args[2]), args[3], Integer.valueOf(args[4]));
			}
			catch(IOException xe){
				Logger.error("Error creating peer, leaving..");
				return;
			}
		}
		catch(IOException e){
			Logger.error("IOExpcetion on peer reading, leaving: " + e);
			return;
		}

		
		if(args.length == 5)
			peer.run(null, null);
		else{
			peer.run(args[5], Integer.valueOf(args[6]));
		}

		Registry reg = null;
		try {
			reg = LocateRegistry.createRegistry(1099);
		} catch (RemoteException ex) {
		}
		try{
		reg = LocateRegistry.getRegistry(1099);
		}
		catch(RemoteException e){
			return;
		}

		try {
			ChordRemote stub = (ChordRemote) UnicastRemoteObject.exportObject(peer, 0);
			reg.rebind(id, stub);
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(1);
		}

		final Peer st = peer;
		Runtime.getRuntime().addShutdownHook(new Thread(
								() -> {
										try {
											Logger.info("Saving peer to disk");
											File peerFile = new File(st.getId()+".peer");

											FileOutputStream fileOutput = new FileOutputStream(peerFile);
											ObjectOutputStream objectOutput = new ObjectOutputStream(fileOutput);
											objectOutput.writeObject(st);
											Logger.info("Peer has been saved to disk");
										} catch (IOException e) {
											e.printStackTrace();
										}	
											
								}

								));
			


	}

	@Override
	public String state() throws RemoteException {
		StringBuilder builder = new StringBuilder();
		builder.append("Predecessor:\n");
		if (predecessor != null) {
			builder.append("\t").append(predecessor);
		} else {
			builder.append("\t").append("none");
		}
		builder.append("\nFinger table:\n");
		for (int i = 0; i < fingers.length(); i++) {
			if (fingers.get(i) != null) {
				builder.append("\t").append(i).append(" - ").append(fingers.get(i).toString()).append("\n");
			}
		}
		builder.append("Backed up files:\n");
		idToFile.forEach((id, file) -> {
			builder.append("\t").append(file.getFilePath()).append(":\n");
			builder.append("\t\t").append("ID: ").append(file.getId()).append("\n");
			builder.append("\t\t").append("Replication degree: ").append(file.getDesiredReplicationDegree()).append("\n");
			builder.append("\t\t").append("Number of chunks: ").append(file.numChunks()).append("\n");
			if (file.allChunksWithDesiredReplication()) {
				builder.append("\t\t").append("All chunks are properly backed up.\n");
			}
			else
				builder.append("\t\t").append("Some chunks are not properly backed up.\n");
		});

		final AtomicLong usedSpace = new AtomicLong(0);
		builder.append("Saved chunks:\n");
		long maxSpace = storageManager.runOnAllChunks((data) -> {

			usedSpace.getAndAdd(data.getSize());
			builder.append("\t").append("ID - ").append(data.getUniqueIdentifier()).append(":\n");
			builder.append("\t\t").append("Size: ").append(data.getSize()).append("\n");
			builder.append("\t\t").append("Desired replication degree: ").append(data.getDesiredReplicationDegree()).append("\n");
			builder.append("\t\t").append("Apparent replication degree: ").append(data.getApparentReplicationDegree()).append("\n");
			return null;
		});

		builder.append("Space: " + (usedSpace.get() * 1.0f) / Constants.CHUNK_MAX_SIZE_BYTES + "/" + maxSpace / Constants.CHUNK_MAX_SIZE_BYTES + " MiB\n");
		return builder.toString();
	}


    private void buildFingerTable(){
        
    }

	private boolean isInBetween(Long lowerBound, Long upperBound, Long object){

		Logger.debug("bounds are " + lowerBound + " " + upperBound);
		if(upperBound < lowerBound){
			return (object <= upperBound || lowerBound <= object);

		}
		else{
			return object >= lowerBound && object <= upperBound;
		}
	}

	private Finger closest_preceding_node(String key){
		

		for(int i = fingers.length()-1; i>=0; i--){
				Finger currentFinger = fingers.get(i);
				if(currentFinger == null)
					continue;

				if(key.equals(ownFinger.getId()) || key.equals(currentFinger.getId()))
					continue;
				//Finger is between current and next finger
				if(isInBetween(ownFinger.getIdLong(), Long.parseLong(key,16), currentFinger.getIdLong())){
					return currentFinger;
				}
		}

		return ownFinger;

	}

    public void find_successor(String key, Function<Finger, Void> callback){
		find_successor(key, callback, ()->{
			Logger.error("find sucessor failed");
		});
    }

    public void find_successor(String key, Function<Finger, Void> callback, Runnable runOnFailure){
		Function<Finger, Void> onFound = (finger) -> {

			if(finger.equals(ownFinger)){
				callback.apply(finger);
			}
			else{
				find_successor_peer(finger.getAddress(), key, callback, runOnFailure);
			}
			return null;
		};


        //Successor of n
		Finger successor = fingers.get(0);
		if(successor != null && !ownFinger.getId().equals(key) && isInBetween(ownFinger.getIdLong(), successor.getIdLong(), Long.parseLong(key, 16))){

			callback.apply(successor);
			return;
		}

		onFound.apply(closest_preceding_node(key));
    }



	public void connectToPeer(InetSocketAddress peerAddress, Function<SSLConnectionManager, Void> callback){
		connectToPeer(peerAddress, callback, ()->{});
	}
	private void connectToPeer(InetSocketAddress peerAddress, Function<SSLConnectionManager, Void> callback, Runnable runOnFailure){
		AsynchronousSocketChannel ch = null;
		try{
			ch = AsynchronousSocketChannel.open();
		}	
		catch(IOException e){
			Logger.error("Exception when opening");
			return;
		}

		final Peer current = this;
		final AsynchronousSocketChannel capturedChannel = ch;
		CompletionHandler<Void, Void> inlined = new CompletionHandler<Void, Void>() {
				@Override
				public void completed(Void result, Void obj) {
					SSLConnectionManager sslConn = new SSLConnectionManager(capturedChannel, true, current);
					sslConn.setRunOnHandshake(() -> {callback.apply(sslConn);});
					sslConn.runConnection();
				}

				@Override
				public void failed(Throwable exc, Void obj) {
					Logger.error("Error connecting to the given peer of the chord ring: " + peerAddress.toString());
					runOnFailure.run();
				}
			};
		ch.connect(peerAddress, null, inlined);
	}

	public void find_successor_peer(InetSocketAddress peerAddress, String successor, Function<Finger, Void> callback){
		find_successor_peer(peerAddress, successor, callback, ()->{});
	}

	public void find_successor_peer(InetSocketAddress peerAddress, String successor, Function<Finger, Void> callback, Runnable runOnFailure){

		connectToPeer(peerAddress, (manager)->
				{
					try{
						ByteArrayOutputStream out = new ByteArrayOutputStream();
						ObjectOutputStream os = new ObjectOutputStream(out);
						os.writeObject(new Message(MessageType.FIND_SUCCESSOR, successor));

						Runnable onSuccess = () ->
						{
							ResizableBuffer buf = new ResizableBuffer();
							manager.read(buf, ()->{
								try{
									ByteArrayInputStream in = new ByteArrayInputStream(buf.array(), 0, buf.position());
									ObjectInputStream is = new ObjectInputStream(in);
									callback.apply((Finger)is.readObject());
									Logger.success("find_successor_peer finished!");
								}
								catch(IOException e){
									Logger.error("Problem de-serializing object: find_successor_peer");
									runOnFailure.run();
								}
								catch(ClassNotFoundException e){
									Logger.error("Problem class not found: find_successor_peer");
									runOnFailure.run();
								}
							   	manager.close();
							},
						   	() -> { runOnFailure.run(); Logger.error("Could not read data from find_sucessor");});
						};

						manager.write(ByteBuffer.wrap(out.toByteArray()),
								onSuccess	   
								,
								()->{ runOnFailure.run(); Logger.error("Could not send object to join chord network");});
					}
					catch(IOException e){
						Logger.error("Could not serialize object to join chord network");
						runOnFailure.run();
					}

					return null;
				},
				runOnFailure
				);

	}

    /**
     * Join a Chord ring containing node n'
     */
    public void join(InetSocketAddress peerAddress) {
        predecessor = null;
		find_successor_peer(peerAddress, ownFinger.getId(), (finger) -> { Logger.info("Join got: " + finger); fingers.set(0, finger); return null; });
    }

	public void notify_peer(Runnable runAlways){
		Finger successor = fingers.get(0);
		if(successor == null){
			Logger.info("Sucessor was NULL! Didn't notify");
			return;
		}

		connectToPeer(successor.getAddress(), (manager)->
				{
					try{
						ByteArrayOutputStream out = new ByteArrayOutputStream();
						ObjectOutputStream os = new ObjectOutputStream(out);
						os.writeObject(new Message(MessageType.NOTIFY, ownFinger));

						manager.write(ByteBuffer.wrap(out.toByteArray()),
								()->{Logger.success("Notified with success!"); runAlways.run();}
								,
								()->{Logger.error("Notify failed"); runAlways.run();});
					}
					catch(IOException e){
						Logger.error("Could not serialize object to notify chord network");
						runAlways.run();
					}

					return null;
				});
	}

    /**
     * Called periodically.
     * Verifies n's immediate successor, and tells the successor about n
     */
    public void stabilize() {

		final Finger successor = fingers.get(0);
		Logger.info("Stabilize: current sucessor " + successor.getId() + " predecessor " + predecessor);
		if(successor == null){
			fingers.set(0, ownFinger);
			return;
		}

		if(successor.equals(ownFinger)){
			if(predecessor != null)
				fingers.set(0, predecessor);
			return;
		}

		connectToPeer(successor.getAddress(), (manager)->
				{
					try{
						ByteArrayOutputStream out = new ByteArrayOutputStream();
						ObjectOutputStream os = new ObjectOutputStream(out);
						os.writeObject(new Message(MessageType.PREDECESSOR, null));

						Runnable onSuccess = () ->
						{
							ResizableBuffer buf = new ResizableBuffer();
							manager.read(buf, ()->{
								try{
									ByteArrayInputStream in = new ByteArrayInputStream(buf.array(), 0, buf.position());
									ObjectInputStream is = new ObjectInputStream(in);
									Finger curPred = ((Finger)is.readObject());


									if(curPred != null){
										boolean isOwn = curPred.equals(ownFinger);
										boolean isSucc = curPred.equals(successor);


										if(isOwn || isSucc){
										}
										else if (!isOwn && !isSucc && isInBetween(ownFinger.getIdLong(), successor.getIdLong(), curPred.getIdLong())) {
											fingers.set(0, curPred);
										}
									}

									notify_peer(()->{});
								}
								catch(IOException e){
									Logger.error("Problem de-serializing object: stabilize");
								}
								catch(ClassNotFoundException e){
									Logger.error("Problem class not found: stabilize");
								}
							   	manager.close();
							},
						   	() -> { 
								Logger.error("Could not read data from find_sucessor");
							});
						};

						manager.write(ByteBuffer.wrap(out.toByteArray()),
								onSuccess	   
								,
								()->{
									Logger.error("Could not send object to join chord network");
								});
					}
					catch(IOException e){
						Logger.error("Could not serialize object to stabilize chord network");
					}

					return null;
				},
			() -> {
				fingers.set(0, ownFinger);
				}
			
			);

    }

    /**
     * N' thinks it might be our predecessor
     * @param newPred Id of node N'
     */
    @Override
    public void notify(Finger newPred){
		if(newPred == null)
			return;

		if(predecessor == null){
			Logger.success("Set new predecessor, last was null");
			predecessor = newPred;
		}
		else if( (!newPred.equals(ownFinger) && !newPred.equals(predecessor)) && isInBetween(predecessor.getIdLong(), ownFinger.getIdLong(), newPred.getIdLong())){
			Logger.success("Set new predecessor, last was outdated");
			predecessor = newPred;
		}

    }

    /**
     * Called periodically
     * Refreshes finger tables entries
     */
    @Override
    public void fix_fingers(){
		if(ownFinger == null)
			return;
		int currentNext = next;
		synchronized(this){
			currentNext = next;
			next = (currentNext+1)%Constants.M;
		}

		String current = Long.toHexString(ownFinger.getIdLong() + (long)Math.pow(2, currentNext)).toUpperCase();
		String res = "";
		for(int i = current.length(); i<8; i++){
			res += "0";
		}
		res+=current;

		final int setter = currentNext;
		find_successor(res, (finger)->{ fingers.set(setter, finger); return null;}, ()->
				{
					if(setter == 0)
						fingers.set(setter, ownFinger);
					else
						fingers.set(setter, null);
				});
    }

    /**
     * Called periodically
     * Checks whether predecessor has failed
     */
    @Override
    public void check_predecessor(){
		Logger.info("Checking predecessor!");

		if(predecessor == null){
			return;
		}

		connectToPeer(predecessor.getAddress(), 
				(manager)->{ manager.close();
				   	return null;},
				()->{predecessor = null; Logger.error("Predecessor seems dead resetting"); });
    }

    /**
     * Creates a new Chord ring
     */
    public void create() {
        predecessor = null;
        // Creates the current node
		Logger.info("Creating chord, i'm peer: " + ownFinger.getId());
		fingers.set(0, ownFinger);
    }


	public void addTask(Runnable r){
		tPool.submit(r);
	}

	public Future addTaskWithDelay(Runnable r, long delay){
		return addTaskWithDelay(r, delay , TimeUnit.MILLISECONDS);
	}

	public Future addTaskWithDelay(Runnable r, long delay, TimeUnit tu) {
    	return tPool.schedule(r, delay, tu);
	}

	public static String hashAlgorithm(byte[] convertme) {
		MessageDigest md = null;
		try {
			md = MessageDigest.getInstance("SHA-256");
		}
		catch(NoSuchAlgorithmException e) {
			throw new RuntimeException("We ded");
		} 

		byte[] arr = md.digest(convertme);
		byte[] res = new byte[4];
		System.arraycopy(arr, 0, res, 0, 4);
		StringBuilder sb = new StringBuilder();
		for (byte b : res) {
				sb.append(String.format("%02X", b));
		}
		
		return sb.toString();
	}


	public Finger getPredecessor(){
		return predecessor;
	}

	@Override
	public String getId(){
		return ownFinger.getId();
	}

	public Finger findEarliestFreeFinger(){

		for(int i=0;i<fingers.length();i++){
			Finger cur = fingers.get(i);
			if(cur == null)
				continue;

			if(cur.equals(ownFinger))
				continue;

			return cur;
		}
		return null;
	}

	public Finger getOwnFinger(){
		return ownFinger;
	}

	private String peerDir(){
		return this.getId()+"/";
	}

	public String restoreDir(){
		return peerDir();
	}

	private String chunksDir(){
		return peerDir() + "chunks/";
	}

	public String chunkToPath(ChunkData chData){
		return  chunksDir() + chData.getUniqueIdentifier();
	}
	private void awaitYeet(SSLConnectionManager connection, ResizableBuffer buffer) {
		connection.read(buffer, () -> {
			Message msg = null;
			try{
				ByteArrayInputStream in = new ByteArrayInputStream(buffer.array(), 0, buffer.position());
				ObjectInputStream is = new ObjectInputStream(in);
				msg = (Message)is.readObject();
			}
			catch(EOFException e){
				awaitYeet(connection, buffer);
			}
			catch(IOException e){
				Logger.error("Problem de-serializing object " + buffer);
				return;
			}
			catch(ClassNotFoundException e){
				Logger.error("Could not find class for de-serialization");
				return;
			}
			if(msg.getType() != MessageType.YEET){
				Logger.warn("YEET: Got wrong answer " + msg.getType().name());
				return;
			}

			YeetPayload payload = (YeetPayload) msg.getBody();
			ChunkData chData = new ChunkData(payload.getPeerID(), payload.getFileId(), payload.getChunkNumber(), 0, 0);

			Logger.info("YEET: Got yeet for " + chData.getUniqueIdentifier());
			if (!storageManager.chunkExist(chData)) {
				Logger.warn("Chunk to YEET doesn't exist.");
				return;
			}

			storageManager.deleteChunk(chData, chunkData -> {
				Message toSend = new Message(MessageType.YEETED, new YeetedPayload(getId(), chunkData.getFileId(), chunkData.getChunkNo()));
				try {
					ByteArrayOutputStream out = new ByteArrayOutputStream();
					ObjectOutputStream os = new ObjectOutputStream(out);
					os.writeObject(toSend);
					ByteBuffer buf = ByteBuffer.wrap(out.toByteArray());
					Logger.info("YEETED: Sendint to " + chData.getSenderId());
					connection.write(buf, connection::close, ()->{});
				}
				catch(IOException e){
					Logger.error("AWAIT YEET - Problem serializing YEETED message");
					connection.close();
				}
				return null;
			});
		}, () -> {});

	}

	private void awaitYeeted(SSLConnectionManager connection, ResizableBuffer buffer) {
		connection.read(buffer, () -> {
			Message msg = null;
			try{
				ByteArrayInputStream in = new ByteArrayInputStream(buffer.array(), 0, buffer.position());
				ObjectInputStream is = new ObjectInputStream(in);
				msg = (Message)is.readObject();
			}
			catch(EOFException e){
			    awaitYeeted(connection, buffer);
			}
			catch(IOException e){
				Logger.error("Problem de-serializing object " + buffer);
				return;
			}
			catch(ClassNotFoundException e){
				Logger.error("Could not find class for de-serialization");
				return;
			}
			if(msg.getType() != MessageType.YEETED){
				Logger.warn("YEETED: Got wrong answer " + msg.getType().name());
				return;
			}
			YeetedPayload payload = (YeetedPayload) msg.getBody();
			BackedupFile backedUp = idToFile.get(payload.getFileId());
			if (backedUp == null) {
				Logger.error("Received YEETED for non existant file.");
				return;
			}
			backedUp.removeChunkStored(new ChunkData(payload.getPeerId(), payload.getFileId(), payload.getChunkNumber(), 0 ,0));
		}, () -> {});
	}

	public void stored(SSLConnectionManager connection, StoredPayload payload) {
		BackedupFile backedUp = idToFile.get(payload.getFileId());
		if(backedUp == null){
			Logger.warn("Got STORED for file I dont own");
			return;
		}
		
		ChunkData chData = new ChunkData(payload.getPeerId(), payload.getFileId(), payload.getChunkNo(), 0, 0);
		int numChunksReplicator = 0;
		int oldNumReplicators = 0;
		synchronized(this){
			oldNumReplicators = backedUp.getReplicatorsForChunk(payload.getChunkNo()).size();
			numChunksReplicator = backedUp.addChunkStored(chData);
		}

        if (oldNumReplicators < numChunksReplicator && 
				numChunksReplicator > backedUp.getDesiredReplicationDegree()) {
			Message msg = new Message(MessageType.YEET, new YeetPayload(getId(), backedUp.getId(), payload.getChunkNo()));
			try {
				ByteArrayOutputStream out = new ByteArrayOutputStream();
				ObjectOutputStream os = new ObjectOutputStream(out);
				os.writeObject(msg);
				ByteBuffer buf = ByteBuffer.wrap(out.toByteArray());
				Logger.info("YEET: Sending to " + payload.getPeerId());

				connection.write(buf, () -> awaitYeeted(connection, new ResizableBuffer()), ()->{});
			}
			catch(IOException e) {
				Logger.error("STORED - Problem serializing YEET message");
				connection.close();
			}
		}
		else{
			Logger.debug("YEET: " + payload.getPeerId() + " does not need to be yeeted! " + oldNumReplicators + " " + numChunksReplicator);
			connection.close();
		}
	}

	public void reclaim(long newSize) throws RemoteException{
		if(newSize < 0)
			throw new RemoteException("Size should be positive");

		storageManager.reclaimSpace(newSize*Constants.CHUNK_MAX_SIZE_BYTES);
	}

	public void getchunk(Function<Object, Void> answer, ChunkData chData){
		if(!storageManager.chunkExist(chData)){
			answer.apply(new Message(MessageType.REMOVED, null));
		}
		else{
			storageManager.getPhysicalChunk(chData, (buffer)->{
				byte[] newBuf = new byte[buffer.position()];
				System.arraycopy(buffer.array(), 0, newBuf, 0, newBuf.length);
				answer.apply(new Message(MessageType.CHUNK, new ChunkPayload(newBuf)));
				return null;
			},
			()->{});
				
		}
	}

	public void removed(RemovedPayload payload){
		if(!payload.getOwnerId().equals(getId())){
			Logger.warn("Got removed for other owner " + payload.getOwnerId());
			return;
		}

		ChunkData chData = payload.toChunkData();
		BackedupFile backedup = idToFile.get(chData.getFileId());
		if(backedup == null)
			return;

		backedup.removeChunkStored(chData);
	}


	public void deleteChunks(DeletePayload payload, Function<Long, Void> sender){

		Function<ChunkData, Void> translator = (chData)-> {
			Logger.info("Sending REMOVED_DELETE for chunk: " + chData.getUniqueIdentifier());
			payload.removeElement(chData.getChunkNo());
			sender.apply(chData.getChunkNo());
			return null;
		};

		storageManager.deleteFile(payload.getFileId(), translator);

		while(payload.missingChunks()){
			sender.apply(payload.getChunk());
		}
	}

	public boolean gotBegged(ChunkData chData){

		if(!storageManager.chunkExist(chData)){
			Logger.info("BEGGED: Chunk doesn't exist here anymore " + chData.getUniqueIdentifier());
			return false;
		}


		Finger nearestFinger = findEarliestFreeFinger();
		if(nearestFinger == null){
			Logger.info("BEGGED: No nearest finger to send it to " + chData.getUniqueIdentifier());
			return false;
		}

		Logger.info("BEGGED: Will try to send to " + nearestFinger);
		storageManager.getPhysicalChunk(
				chData,
				(buffer)->{

					connectToPeer(nearestFinger.getAddress(),
							(connection)->{
								byte[] newBuf = new byte[buffer.position()];
								System.arraycopy(buffer.array(), 0, newBuf, 0, newBuf.length);
								Message msg = new Message(MessageType.PUTCHUNK, new PutChunkPayload(
											chData.getSenderId(),
											10,
											chData.getDesiredReplicationDegree(),
											chData.getApparentReplicationDegree(),
											chData.getFileId(),
											chData.getChunkNo(),
											newBuf
											));
								try{
									ByteArrayOutputStream out = new ByteArrayOutputStream();
									ObjectOutputStream os = new ObjectOutputStream(out);
									os.writeObject(msg);
									connection.write(ByteBuffer.wrap(out.toByteArray()), ()->{
										
										Logger.success("BEGGED: Sent " + chData.getUniqueIdentifier() + " to nearest finger! " + nearestFinger);
										connection.close();}, ()->{});	
								}
								catch(IOException e){
									Logger.error("BEGGED: Problem serializing object");
								}
								return null;
							},
							()->{Logger.error("BEGGED: Failed to connect to " + nearestFinger);});

					return null;
				},
				()->{}
				);
		return true;
	}



}
