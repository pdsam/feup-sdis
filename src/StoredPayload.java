import java.io.Serializable;

public class StoredPayload implements Serializable{
    private String peerId;
	private String fileId;
	private Long chunkNo;

    public StoredPayload(String peerId, String fileId, Long chunkNo) {
        this.peerId = peerId;
		this.fileId = fileId;
		this.chunkNo = chunkNo;
    }

    public String getPeerId() {
        return peerId;
    }

	public String getFileId(){
		return fileId;
	}

	public Long getChunkNo(){
		return chunkNo;
	}
}
