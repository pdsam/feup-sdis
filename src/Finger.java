import java.net.InetSocketAddress;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.io.Serializable;

public class Finger implements Serializable{
    private String id;
	private InetSocketAddress addr;


    Finger(String id, InetSocketAddress addr){
        this.id = id;
		this.addr =addr;
    }

    public String getId(){
        return id;
    }

    public Long getIdLong(){
        return Long.parseLong(id,16);
    }

	public InetSocketAddress getAddress(){
		return addr;
	}

	public String toString(){
		return addr.toString() + " " + id;
	}

	@Override
	public boolean equals(Object o){

		if(o == null)
			return false;
		if(!(o instanceof Finger))
			return false;
		return getId().equals(((Finger)o).getId());
	}
}
